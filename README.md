# MUBI

## _Applaudo ATP Final Project_

## How to run it?

Open the project using **Android Studio Canary**

## Downloads

| Program | Link |
| ------ | ------ |
| Android Studio Canary | [https://developer.android.com/studio/preview?hl=es-419][PlDb] 

## Special Thanks To
> Roberto Escalon,
> Nelson Quintanilla,
> Rodrigo Miranda,
> David Guevara,
> Edgar Flores,
> Juan Hurtado,
> Juan Garcia,
> Nelson Paulino,
> and my friend Carlos! and the rest of the Applaudo Team!

[PlDb]: <https://developer.android.com/studio/preview?hl=es-419>
 
