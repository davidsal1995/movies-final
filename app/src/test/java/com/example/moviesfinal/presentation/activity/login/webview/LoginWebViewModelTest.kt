package com.example.moviesfinal.presentation.activity.login.webview

import com.example.data.util.test.FakeAuthRepositoryImpl
import com.example.domain.interactor.auth.GetSessionUseCase
import com.example.domain.model.DataResource
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class LoginWebViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val fakeRepository = FakeAuthRepositoryImpl(DataResource.Status.SUCCESS, "Success")

    private val viewModel = LoginWebViewModel(
        GetSessionUseCase(fakeRepository)
    )

    fun testGetSessionStatus() {
        val job = Job()

        viewModel.sessionStatus.observeForever {
            if (it.data.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.requestSession("token")

        runBlocking { job.join() }
    }
}