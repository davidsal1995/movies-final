package com.example.moviesfinal.presentation.activity.seasondetails

import com.example.data.util.test.FakeShowRepositoryImpl
import com.example.domain.interactor.show.GetSeasonDetailsUseCase
import com.example.domain.model.DataResource
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class SeasonDetailsViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val fakeShowRepository = FakeShowRepositoryImpl(DataResource.Status.SUCCESS, "Success")

    private val viewModel = SeasonDetailsViewModel(GetSeasonDetailsUseCase(fakeShowRepository))

    fun testGetDetailsState() {
        val job = Job()

        viewModel.detailsState.observeForever {
            if (it.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.loadDetails(1, 1)

        runBlocking { job.join() }
    }
}