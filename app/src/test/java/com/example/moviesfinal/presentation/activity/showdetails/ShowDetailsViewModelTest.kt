package com.example.moviesfinal.presentation.activity.showdetails

import com.example.data.util.test.FakeAuthRepositoryImpl
import com.example.data.util.test.FakeShowRepositoryImpl
import com.example.domain.interactor.auth.GetAccountUseCase
import com.example.domain.interactor.show.GetShowDetailsUseCase
import com.example.domain.interactor.show.GetShowFavoriteStateUseCase
import com.example.domain.interactor.show.SetShowFavoriteStateUseCase
import com.example.domain.model.DataResource
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class ShowDetailsViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val fakeShowRepository = FakeShowRepositoryImpl(DataResource.Status.SUCCESS, "Success")
    private val fakeAuthRepository = FakeAuthRepositoryImpl(DataResource.Status.SUCCESS, "Success")

    private val viewModel = ShowDetailsViewModel(
        GetShowDetailsUseCase(fakeShowRepository),
        GetShowFavoriteStateUseCase(fakeShowRepository),
        GetAccountUseCase(fakeAuthRepository),
        SetShowFavoriteStateUseCase(fakeShowRepository)
    )

    fun testGetDetailsState() {
        val job = Job()

        viewModel.detailsState.observeForever {
            if (it.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.loadDetails(1)

        runBlocking { job.join() }
    }

    fun testGetFavoriteState() {
        val job = Job()

        viewModel.favoriteState.observeForever {
            if (it.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.loadFavorite(1, "session id")

        runBlocking { job.join() }
    }

    fun testSetFavorite() {
        val job = Job()

        viewModel.favoriteState.observeForever {
            if (it.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.setFavorite(true, "session id", 1) {}

        runBlocking { job.join() }
    }
}