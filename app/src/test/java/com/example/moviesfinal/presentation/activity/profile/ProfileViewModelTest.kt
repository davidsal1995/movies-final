package com.example.moviesfinal.presentation.activity.profile

import com.example.data.util.test.FakeAuthRepositoryImpl
import com.example.data.util.test.FakeShowRepositoryImpl
import com.example.domain.interactor.auth.DeleteSessionUseCase
import com.example.domain.interactor.auth.GetAccountUseCase
import com.example.domain.interactor.show.GetAccountFavoritesUseCase
import com.example.domain.model.DataResource
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class ProfileViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val fakeShowRepository = FakeShowRepositoryImpl(DataResource.Status.SUCCESS, "Success")
    private val fakeAuthRepository = FakeAuthRepositoryImpl(DataResource.Status.SUCCESS, "Success")

    private val viewModel = ProfileViewModel(
        GetAccountUseCase(fakeAuthRepository),
        GetAccountFavoritesUseCase(fakeShowRepository),
        DeleteSessionUseCase(fakeAuthRepository)
    )

    fun testGetProfileState() {
        val job = Job()

        viewModel.profileState.observeForever {
            if (it.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.loadProfile("session")

        runBlocking { job.join() }
    }

    fun testGetFavoritesState() {
        val job = Job()

        viewModel.favoritesState.observeForever {
            if (it.size >= 0) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.loadProfile("token")

        runBlocking { job.join() }
    }

    fun testGetLogOut() {
        val job = Job()

        viewModel.logOut("session id") { if (it) job.complete() }

        runBlocking { job.join() }
    }
}