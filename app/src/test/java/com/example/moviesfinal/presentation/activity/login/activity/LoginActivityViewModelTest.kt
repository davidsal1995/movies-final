package com.example.moviesfinal.presentation.activity.login.activity

import com.example.data.util.test.FakeAuthRepositoryImpl
import com.example.domain.interactor.auth.GetTokenUseCase
import com.example.domain.model.DataResource
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class LoginActivityViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val viewModel = LoginActivityViewModel(
        GetTokenUseCase(
            FakeAuthRepositoryImpl(
                DataResource.Status.SUCCESS,
                "Success"
            )
        )
    )

    fun testGetTokenStatus() {
        val job = Job()

        viewModel.requestToken()

        viewModel.tokenStatus.observeForever {
            if (it.data.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        runBlocking { job.join() }
    }
}