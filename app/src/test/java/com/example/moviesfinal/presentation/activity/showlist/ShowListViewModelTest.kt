package com.example.moviesfinal.presentation.activity.showlist

import com.example.data.util.test.FakeShowRepositoryImpl
import com.example.domain.interactor.show.*
import com.example.domain.model.DataResource
import com.example.moviesfinal.framework.helper.ShowTag
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.rules.Timeout
import java.util.concurrent.TimeUnit

class ShowListViewModelTest : TestCase() {

    @get:Rule
    var timeout: Timeout = Timeout(2, TimeUnit.SECONDS)

    private val fakeShowRepository = FakeShowRepositoryImpl(DataResource.Status.SUCCESS, "Success")
    private val fakeShowRepositoryFail = FakeShowRepositoryImpl(DataResource.Status.ERROR, "Error")

    private val viewModel = ShowListViewModel(
        GetTopRatedUseCase(fakeShowRepository),
        GetPopularUseCase(fakeShowRepository),
        GetOnTvUseCase(fakeShowRepository),
        GetAiringTodayUseCase(fakeShowRepository),
        InsertShowListUseCase(fakeShowRepository),
        GetShowListTagAll(fakeShowRepository)
    )

    private val viewModelFail = ShowListViewModel(
        GetTopRatedUseCase(fakeShowRepositoryFail),
        GetPopularUseCase(fakeShowRepositoryFail),
        GetOnTvUseCase(fakeShowRepositoryFail),
        GetAiringTodayUseCase(fakeShowRepositoryFail),
        InsertShowListUseCase(fakeShowRepositoryFail),
        GetShowListTagAll(fakeShowRepositoryFail)
    )

    fun testGetShowList() {
        val job = Job()

        viewModel.showList.observeForever {
            if (it.data.status == DataResource.Status.SUCCESS) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.getNextPage(ShowTag.Popular)

        runBlocking { job.join() }
    }

    fun testGetGetShowListByTag() {
        val job = Job()

        viewModelFail.showList.observeForever {
            if (it.data.status == DataResource.Status.ERROR) {
                assertTrue(true)
                job.complete()
            }
        }

        viewModel.getNextPage(ShowTag.Popular)

        runBlocking { job.join() }
    }
}