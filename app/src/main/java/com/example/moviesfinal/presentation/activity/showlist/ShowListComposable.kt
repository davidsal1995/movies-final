package com.example.moviesfinal.presentation.activity.showlist

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.domain.model.show.ShowEntry
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.POSTER_URL
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.framework.helper.ShowTag
import com.example.moviesfinal.framework.helper.isTablet
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.ProfileTopBarIcon
import com.example.moviesfinal.presentation.composable.RatingStars
import com.example.moviesfinal.presentation.theme.*
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshIndicator
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * View for a single show entry
 */
@Composable
fun ShowCell(show: ShowEntry, onShowClick: (Int) -> Unit) {
    // Keep track for the item being clicked
    val isClicked = remember { mutableStateOf(false) }
    // Scope to be cancelled when the composition is destroyed
    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier
            .padding(bottom = 16.dp, start = 16.dp)
            .shadow(if (isClicked.value) 8.dp else 4.dp, RoundedCornerShape(12.dp))
            .clip(RoundedCornerShape(12.dp))
            .fillMaxWidth()
            .height(if (LocalContext.current.isTablet()) 350.dp else 263.dp)
            .background(PureWhiteBackGround)
            // Give the item a nice ripple effect
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(
                    bounded = true,
                    color = MaterialTheme.colors.primary
                ),
                // Show a bigger shadow when clicked
                onClick = {
                    coroutineScope.launch {
                        isClicked.value = true
                        delay(200)
                        isClicked.value = false
                    }
                    onShowClick(show.id)
                }
            )
    ) {
        GlideImage(
            imageModel = POSTER_URL + show.posterPath,
            contentScale = ContentScale.FillWidth,
            circularRevealedEnabled = true,
            placeHolder = ImageBitmap.imageResource(R.drawable.entry_placeholder),
            alignment = Alignment.Center,
            modifier = Modifier
                .fillMaxWidth()
                .height(if (LocalContext.current.isTablet()) 250.dp else 170.dp),
            requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        )
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                color = DarkGrayText,
                text = show.name,
                style = MaterialTheme.typography.subtitle2,
                fontSize = 16.sp,
                maxLines = 2,
                modifier = Modifier.padding(horizontal = 12.dp)
            )
            Row(
                modifier = Modifier
                    .padding(start = 9.dp)
                    .padding(top = 2.dp)
            ) {
                RatingStars(show.voteAverage / 2.0, 20.dp, LightCyanStar)
            }
        }
    }
}

/**
 * A swipe to refresh layout containing a
 * simple lazy vertical grid of two columns
 * @param isRefreshing is for showing/hiding the spinner loader
 * @param listState is for keeping track of the current lazy list layout
 * @param onRefresh is a callback when the refresh is started
 * @param onShowClick is a callback when an item is clicked
 * @param showList is the show list to render
 */
@ExperimentalFoundationApi
@Composable
fun ShowListPaging(
    showList: List<ShowEntry>,
    listState: LazyListState,
    isRefreshing: Boolean,
    onRefresh: () -> Unit,
    onShowClick: (Int) -> Unit
) {

    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing),
        onRefresh = onRefresh,
        indicator = { state, trigger ->
            SwipeRefreshIndicator(
                state = state,
                refreshTriggerDistance = trigger,
                scale = true,
                backgroundColor = MaterialTheme.colors.primary,
            )
        }
    ) {
        LazyVerticalGrid(
            cells = GridCells.Fixed(if (LocalContext.current.isTablet()) 5 else 2),
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(end = 16.dp),
            state = listState
        ) {
            items(showList) {
                ShowCell(it, onShowClick)
            }
        }
    }
}

/**
 * Simple chip to show a show tag
 * @param active is the current active tag
 * @param onClick is the callback when the tag is clicked
 * @param tag is the tag to be rendered
 */
@Composable
fun TagChip(tag: ShowTag, active: Boolean, onClick: (ShowTag) -> Unit) {
    OutlinedButton(
        onClick = { onClick(tag) },
        border = BorderStroke(
            1.5.dp,
            if (active) MaterialTheme.colors.primary else DarkGrayBorder
        ),
        shape = RoundedCornerShape(50),
        colors = ButtonDefaults.outlinedButtonColors(backgroundColor = if (active) MaterialTheme.colors.primary else LightGrayBackground),
        modifier = Modifier.padding(end = 12.dp)
    ) {
        Text(
            text = tag.value,
            color = if (active) WhiteText else DarkGrayText,
            style = MaterialTheme.typography.subtitle2,
            fontSize = 17.sp
        )
    }
}

/**
 * Simple lazy that renders horizontally the available tags
 * @param activeTag is the current active tag
 * @param onChange is the callback when a tag is clicked
 */
@Composable
fun TagChipSelector(activeTag: MutableState<ShowTag>, onChange: (ShowTag) -> Unit) {
    LazyRow(
        modifier = Modifier
            .padding(0.dp)
            .fillMaxWidth()
            .padding(vertical = 24.dp)

    ) {
        item { Spacer(modifier = Modifier.width(16.dp)) }
        items(ShowTag.values()) { tag ->
            TagChip(tag, activeTag.value.value == tag.value, onChange)
        }
        item { Spacer(modifier = Modifier.width(4.dp)) }
    }
}

/**
 * Is a nice bottom indicator to notify about the loading of more items into the lazy list
 * @param loading is used for showing/hiding the indicator
 */
@Composable
fun LoadingItemsIndicator(loading: Boolean) {
    val coroutineScope = rememberCoroutineScope()
    val show = remember { mutableStateOf(false) }

    // Run the code everytime the loading state changes
    LaunchedEffect(loading) {
        when (loading) {
            true -> show.value = true
            false -> {
                coroutineScope.launch {
                    delay(500)
                    show.value = false
                }
            }
        }
    }
    if (show.value)
        Row(
            modifier = Modifier
                .padding(24.dp)
                .fillMaxWidth()
                .clip(RoundedCornerShape(8.dp))
                .background(DarkGrayText)
                .padding(vertical = 12.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Loading More",
                color = WhiteText,
                modifier = Modifier.padding(end = 12.dp)
            )
            CircularProgressIndicator(
                color = WhiteBackground,
                modifier = Modifier
                    .size(20.dp),
                strokeWidth = 2.dp
            )
        }
}

/**
 * Activity layout
 * Includes a top bar with a title and the profile button
 * @param content is the child view
 * @param loading is the loading state for the loading more indicator
 */
@Composable
fun ShowListLayout(
    loading: Boolean,
    onProfileClick: () -> Unit,
    content: @Composable () -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Tv Shows",
                        color = WhiteText,
                        style = MaterialTheme.typography.h6,
                        fontSize = 22.sp
                    )
                },
                backgroundColor = Purple,
                contentColor = Color.White,
                actions = {
                    ProfileTopBarIcon(onProfileClick)
                },
                modifier = Modifier.shadow(elevation = 12.dp)
            )
        }, content = {
            AppThemeWrapper(ActivityType.General) {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth()
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(0.dp)
                            .weight(1f)
                    ) { content() }
                    LoadingItemsIndicator(loading)
                }
            }
        }
    )
}