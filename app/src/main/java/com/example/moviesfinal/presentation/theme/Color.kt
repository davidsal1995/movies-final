package com.example.moviesfinal.presentation.theme

import androidx.compose.ui.graphics.Color

/**
 * File for storing Compose Colors
 */

val Purple700 = Color(0xFF3700B3)

val Purple = Color(0xFF6243FF)
val PurpleStatus = Color(0xFF0013CA)

val WhiteText = Color(0xFFFBFAFE)
val WhiteBackground = Color(0xFFF0F2F5)
val PureWhiteBackGround = Color(0xFFFFFFFF)

val LightGrayBackground = Color(0xFFD5D8DB)
val DarkGrayBorder = Color(0xFFC4C4C4)
val DarkGrayText = Color(0xFF6B6B83)
val DarkGrayRatingText = Color(0xFF696969)
val SubtleGrayText = Color(0xFF8C8CA1)

val WebBlue = Color(0xFF032541)
val LightBlue = Color(0xFF31B5FE)
val CyanStar = Color(0xFF21BDCA)
val LightCyanStar = Color(0xFF84DFE2)

val CarrotOrange = Color(0xFFE67E22)

val ErrorRed = Color(0xFFF65164)