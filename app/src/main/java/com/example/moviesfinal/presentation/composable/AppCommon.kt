package com.example.moviesfinal.presentation.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.PlayCircleFilled
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.theme.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController

/**
 * Changes the color of the Status and Navigation bars
 * based on the activity name
 * @param activity is the activity type
 */
@Composable
fun StatusNavBar(activity: ActivityType = ActivityType.General) {
    val systemUiController = rememberSystemUiController()
    var useDarkIcons = false
    val primaryColor = MaterialTheme.colors.primary

    // Color pairs for every activity name
    val colorPair = when (activity) {
        ActivityType.Splash -> Pair(LightBlue, primaryColor)
        ActivityType.Login -> {
            useDarkIcons = true
            Pair(WhiteBackground, WhiteBackground)
        }
        ActivityType.WebView -> Pair(WebBlue, WebBlue)
        ActivityType.General -> Pair(PurpleStatus, primaryColor)
    }

    // Restores colors in case of an activity change on the back stack
    SideEffect {
        systemUiController
            .setSystemBarsColor(
                color = colorPair.first,
                darkIcons = useDarkIcons
            )
        systemUiController.setNavigationBarColor(
            color = colorPair.second,
            darkIcons = useDarkIcons
        )
    }
}

/**
 * Big text title for the app name
 * @param activity is the activity type
 */
@Composable
fun AppTextTitle(activity: ActivityType = ActivityType.General) {
    Text(
        text = stringResource(R.string.Mubi),
        color = if (activity == ActivityType.Splash) WhiteText else MaterialTheme.colors.primary,
        fontWeight = FontWeight.Black,
        fontSize = 60.sp,
        modifier = Modifier.padding(start = if (activity == ActivityType.Splash) 12.dp else 0.dp)
    )
}

/**
 * App icon using native components
 * @param splash selects between colorful and mainly white
 */
@Composable
fun AppIcon(splash: Boolean = false) {
    // Show a mainly white app icon
    if (splash) Box(
        modifier = Modifier
            .clip(CircleShape)
            .wrapContentSize()
            .background(Color.Transparent)
            .border(12.dp, Purple.copy(0.63f), CircleShape),
    ) {
        Icon(
            Icons.Filled.PlayCircleFilled,
            stringResource(R.string.Play_Icon),
            modifier = Modifier
                .size(100.dp)
                .padding(10.dp),
            tint = WhiteText
        )
    }
    // Show a colorful app icon
    else {
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .size(100.dp)
                .background(color = Purple.copy(0.4f)),
            contentAlignment = Alignment.Center
        ) {
            Box(
                modifier = Modifier
                    .clip(CircleShape)
                    .size(75.dp)
                    .background(color = WhiteBackground),
                contentAlignment = Alignment.Center
            ) {
                Box(
                    modifier = Modifier
                        .clip(CircleShape)
                        .size(65.dp)
                        .background(brush = PurpleBlueGradient),
                    contentAlignment = Alignment.Center
                ) {
                    Icon(
                        Icons.Filled.PlayArrow,
                        stringResource(R.string.Play_Icon),
                        modifier = Modifier
                            .size(50.dp),
                        tint = WhiteBackground
                    )
                }
            }
        }
    }
}

/**
 * App theme wrapper for the activities
 * @param activity is the activity type
 * @param content is the children composable
 */
@Composable
fun AppThemeWrapper(
    activity: ActivityType = ActivityType.General,
    content: @Composable () -> Unit
) {
    MoviesFinalTheme {
        StatusNavBar(activity)
        when (activity) {
            ActivityType.General, ActivityType.Login -> Surface(
                color = MaterialTheme.colors.background
            ) {
                content()
            }
            else -> content()
        }
    }
}