package com.example.moviesfinal.presentation.activity.login.webview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.auth.GetSessionUseCase
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Session
import com.example.moviesfinal.framework.helper.HandledData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model for the web view login
 * @param getSession is the get session use case
 */
class LoginWebViewModel(
    val getSession: GetSessionUseCase
) : ViewModel() {

    // Live data for the session
    val sessionStatus = MutableLiveData<HandledData<DataResource<Session>>>()

    /**
     * Request a new session using the API
     * @param token is the TMDB token
     */
    fun requestSession(token: String) {
        viewModelScope.launch(Dispatchers.IO) { sessionStatus.postValue(HandledData(getSession(token))) }
    }
}