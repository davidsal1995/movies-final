package com.example.moviesfinal.presentation.activity.seasondetails

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.domain.model.show.EpisodeDetails
import com.example.domain.model.show.SeasonDetails
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.POSTER_URL
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.BackTopBar
import com.example.moviesfinal.presentation.theme.DarkGrayText
import com.example.moviesfinal.presentation.theme.PureWhiteBackGround
import com.skydoves.landscapist.glide.GlideImage

/**
 * Simple episode item with a poster, the name and a little overview
 * @param episode is the episode number
 * @param poster is the episode poster
 */
@Composable
fun EpisodeEntry(episode: EpisodeDetails, poster: String) {
    Row(
        modifier = Modifier
            .height(180.dp)
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .shadow(4.dp, RoundedCornerShape(8.dp))
            .clip(RoundedCornerShape(8.dp))
            .fillMaxWidth()
            .background(PureWhiteBackGround)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(
                    bounded = true,
                    color = MaterialTheme.colors.primary
                ),
                onClick = {}
            )
    ) {
        GlideImage(
            imageModel = "$POSTER_URL${with(episode.stillPath) { if (isEmpty()) poster else this }}",
            contentScale = ContentScale.Crop,
            circularRevealedEnabled = true,
            placeHolder = ImageBitmap.imageResource(R.drawable.entry_placeholder),
            alignment = Alignment.Center,
            modifier = Modifier
                .width(110.dp)
                .fillMaxHeight(),
        )
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Episode ${episode.episodeNumber}",
                color = DarkGrayText,
                style = MaterialTheme.typography.h6,
                fontSize = 22.sp,
                maxLines = 1,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            Text(
                text = with(episode.overview) { if (this.isEmpty()) "No overview available. " else this },
                color = DarkGrayText,
                style = MaterialTheme.typography.body2,
                fontSize = 16.sp,
                maxLines = 3,
                modifier = Modifier.padding(bottom = 8.dp)
            )
        }
    }
}

/**
 * A lazy column for rendering the episode items
 * @param episodes is the episode list
 * @param poster is the season poster in case the episode doesn't have a poster
 */
@Composable
fun EpisodeList(episodes: List<EpisodeDetails>, poster: String) {
    LazyColumn {
        item { Spacer(modifier = Modifier.height(12.dp)) }
        items(episodes) { EpisodeEntry(it, poster) }
        item { Spacer(modifier = Modifier.height(12.dp)) }
    }
}

/**
 * Activity layout with a Top bar and a list of episodes
 * @param details is the season details
 * @param onBack is the callback for the top bar back icon press
 * @param poster is the show poster in case the season doesn't have it's own poster
 */
@Composable
fun SeasonDetailsLayout(details: SeasonDetails?, poster: String, onBack: () -> Unit) {
    details?.let {
        AppThemeWrapper {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
            ) {
                BackTopBar(text = details.name, onBack)
                EpisodeList(details.episodes, poster)
            }
        }
    }
}