package com.example.moviesfinal.presentation.composable

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.WifiOff
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.theme.DarkGrayRatingText
import com.example.moviesfinal.presentation.theme.DarkGrayText
import com.example.moviesfinal.presentation.theme.WhiteBackground

/**
 * Generates a view for the loading state
 * @param activity is the activity type
 */
@Composable
fun LoadingScreen(activity: ActivityType = ActivityType.General) {
    AppThemeWrapper(activity) {
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }
}

/**
 * Generates a view for an error state
 * @param activity is the activity type
 * @param onClick is the callback for the retry button
 */
@Composable
fun ErrorScreen(activity: ActivityType = ActivityType.General, onClick: () -> Unit) {
    AppThemeWrapper(activity) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                Icons.Filled.WifiOff,
                stringResource(R.string.Wifi_Off),
                tint = DarkGrayRatingText,
                modifier = Modifier.size(70.dp)
            )

            Text(
                stringResource(R.string.No_Internet),
                color = DarkGrayText,
                fontSize = 20.sp,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier.padding(top = 40.dp, bottom = 12.dp)
            )

            Text(
                stringResource(R.string.Check_Settings),
                color = DarkGrayText,
                fontSize = 18.sp,
                modifier = Modifier.padding(bottom = 40.dp)
            )

            Button(
                modifier = Modifier
                    .width(150.dp),
                onClick = onClick,
                colors = ButtonDefaults.textButtonColors(
                    backgroundColor = MaterialTheme.colors.primary
                )
            ) {
                Text(
                    text = stringResource(R.string.Retry),
                    fontSize = 17.sp,
                    color = WhiteBackground,
                )
            }

        }
    }
}