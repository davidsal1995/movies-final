package com.example.moviesfinal.presentation.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.moviesfinal.R
import com.example.moviesfinal.presentation.theme.Purple
import com.example.moviesfinal.presentation.theme.WhiteText

/**
 * Simple touchable back icon button
 * @param onBack is the onclick callback
 * @param paddingLeft is the left padding for the button
 */
@Composable
fun BackTopBarIcon(paddingLeft: Dp = 0.dp, onBack: () -> Unit) {
    Icon(
        Icons.Filled.ArrowBack,
        stringResource(R.string.Back),
        Modifier
            .padding(start = paddingLeft)
            .clip(CircleShape)
            .size(28.dp)
            .clickable(onClick = onBack),
        WhiteText
    )
}

/**
 * Button icon for the app top bar
 * @param onClick is the callback for the clickable
 */
@Composable
fun ProfileTopBarIcon(onClick: () -> Unit = {}) {
    Icon(
        Icons.Filled.AccountCircle,
        stringResource(R.string.Back),
        Modifier
            .padding(horizontal = 12.dp)
            .clip(CircleShape)
            .size(28.dp)
            .clickable(onClick = onClick),
        WhiteText
    )
}

/**
 * Simple Top Bar with a back icon
 * @param onBack is the callback for the BackTopBarIcon
 * @param text is the top bar title
 */
@Composable
fun BackTopBar(text: String, onBack: () -> Unit) {
    TopAppBar(
        title = {
            Text(
                text = text,
                color = WhiteText,
                style = MaterialTheme.typography.h6,
                fontSize = 22.sp,
                maxLines = 1
            )
        },

        backgroundColor = Purple,
        contentColor = Color.White,
        actions = {},
        modifier = Modifier.shadow(elevation = 12.dp),
        navigationIcon = { BackTopBarIcon(12.dp, onBack) }
    )
}