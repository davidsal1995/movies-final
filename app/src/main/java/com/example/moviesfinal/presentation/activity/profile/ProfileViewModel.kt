package com.example.moviesfinal.presentation.activity.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.auth.DeleteSessionUseCase
import com.example.domain.interactor.auth.GetAccountUseCase
import com.example.domain.interactor.show.GetAccountFavoritesUseCase
import com.example.domain.model.DataResource
import com.example.domain.model.account.Account
import com.example.domain.model.auth.LogOutRequestBody
import com.example.domain.model.show.ShowDetails
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(
    val getAccount: GetAccountUseCase,
    val getFavorites: GetAccountFavoritesUseCase,
    val deleteSession: DeleteSessionUseCase
) : ViewModel() {
    // Emit live data events with the profile data
    val profileState = MutableLiveData<DataResource<Account>>()
    val favoritesState = MutableLiveData<List<ShowDetails>>()

    /**
     * Load the profile data
     * @param resume to know is the call is from the onCreate or from the onResume
     * @param session is the TMDB session
     */
    fun loadProfile(session: String, resume: Boolean = false) {
        // Emit a loading
        if (!resume) profileState.value = DataResource.loading()
        favoritesState.postValue(listOf())

        // Get the account details and the favorite list
        viewModelScope.launch(Dispatchers.IO) {
            val account = getAccount(session)
            val favorites = getFavorites(account.data?.id ?: -1, session)

            profileState.postValue(account)
            favoritesState.postValue(favorites.data?.results)
        }
    }

    /**
     * Deletes a session from the TMDB API
     * @param callBack is the callback for the result
     * @param session is the TMDB session
     */
    fun logOut(session: String, callBack: (Boolean) -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            val success = deleteSession(LogOutRequestBody(session))

            if (success.status == DataResource.Status.SUCCESS) callBack(
                success.data?.success ?: false
            )
            else callBack(false)
        }
    }
}