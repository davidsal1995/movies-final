package com.example.moviesfinal.presentation.composable

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarHalf
import androidx.compose.material.icons.outlined.StarOutline
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.moviesfinal.R
import com.example.moviesfinal.presentation.theme.CyanStar
import com.example.moviesfinal.presentation.theme.DarkGrayRatingText

/**
 * Converts the rating into stars quantity
 * @param rating is the rating in a scale of 10
 */
fun ratingToStars(rating: Double): Triple<Int, Double, Int> {
    val fullStars = rating.toInt()
    val halfStar = rating - fullStars
    val emptyStars = (5 - rating).toInt()
    return Triple(fullStars, halfStar, emptyStars)
}

/**
 * Generates an Star icon
 * @param color is the star color
 * @param icon is the star icon bitmap
 * @param size is the star size
 */
@Composable
fun StarIcon(icon: ImageVector, size: Dp, color: Color) {
    Icon(
        icon,
        stringResource(R.string.Rating_Star),
        modifier = Modifier
            .size(size),
        tint = color
    )
}

/**
 * Creates a list of 5 stars
 * @param color is the stars color
 * @param label shows or hides the label in a scale of 5
 * @param rating is the rating in a scale of 10
 * @param size is the stars size
 */
@Composable
fun RatingStars(rating: Double, size: Dp = 20.dp, color: Color = CyanStar, label: Boolean = true) {

    val stars = ratingToStars(rating)

    // Generates filled stars
    for (i in 1..stars.first) {
        StarIcon(Icons.Filled.Star, size, color)
    }

    // Generates an empty or half star
    if (stars.second > 0) {
        StarIcon(
            when {
                stars.second < 0.5 -> Icons.Outlined.StarOutline
                else -> Icons.Filled.StarHalf
            },
            size,
            color
        )
    }

    // Generates empty stars
    for (i in 1..stars.third) {
        StarIcon(Icons.Outlined.StarOutline, size, color)
    }

    // Rating in a scale of 5
    if (label)
        Text(
            color = DarkGrayRatingText,
            text = rating.toString().substring(0, 3),
            style = MaterialTheme.typography.body2,
            fontSize = 16.sp,
            maxLines = 2,
            modifier = Modifier.padding(start = 19.dp)
        )
}