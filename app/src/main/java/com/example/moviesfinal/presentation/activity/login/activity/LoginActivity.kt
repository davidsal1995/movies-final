package com.example.moviesfinal.presentation.activity.login.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.lifecycle.lifecycleScope
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Token
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_TOKEN
import com.example.moviesfinal.framework.extension.getSessionId
import com.example.moviesfinal.framework.extension.getSharedPreferencesInstance
import com.example.moviesfinal.framework.helper.HandledData
import com.example.moviesfinal.presentation.activity.login.webview.LoginWebViewActivity
import com.example.moviesfinal.presentation.activity.showlist.ShowListActivity
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : ComponentActivity() {

    private val viewModel: LoginActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Get the session ID
        val sharedPreferences = this.getSharedPreferencesInstance()
        val sessionId = sharedPreferences.getSessionId()

        // if session exists, go to the show list activity
        if (sessionId.isNotEmpty()) openShowList()

        setContent {
            // used for showing the button or the loading spinner
            val loading = remember { mutableStateOf(sessionId.isNotEmpty()) }
            // snack bar host state
            val snackBarHost = remember { mutableStateOf(SnackbarHostState()) }
            // observe the live data as a state
            val tokenStatus by viewModel.tokenStatus.observeAsState()

            tokenStatus?.let {
                // Handle the token sent by the view model
                tokenRequested(it) { result, message ->
                    if (!result) lifecycleScope.launch { snackBarHost.value.showSnackbar(message) }
                    loading.value = false
                }
            }

            // Activity layout
            LoginActivityLayout(
                loading = loading.value,
                snackBarHost = snackBarHost.value,
                onClick = {
                    loading.value = true
                    viewModel.requestToken()
                }
            )
        }
    }

    /**
     * Handles the token sent by the view model
     * @param handledToken is a flag for checking if the data has already been handled
     * @param resolve is a callback to execute with the handling status
     */
    private fun tokenRequested(
        handledToken: HandledData<DataResource<Token>>,
        resolve: (Boolean, String) -> Unit
    ) {
        // Only if the data has not been handled yet
        if (!handledToken.isHandled()) {
            handledToken.setHandled()

            val token = handledToken.data

            // Fail with an error
            if (token.status == DataResource.Status.ERROR)
                resolve(false, getString(R.string.Check_Connection))
            else {
                val successToken = token.data?.success ?: false
                val requestToken = token.data?.request_token ?: ""

                // Fail if the token is not valid
                if (!successToken || requestToken.isEmpty()) resolve(
                    false,
                    getString(R.string.Invalid_Token)
                )
                // Succeed and open the login web view activity
                else {
                    resolve(true, getString(R.string.Success))
                    openWebView(requestToken)
                }
            }
        }
    }

    /**
     * Opens the login web view activity
     * @param token is the TMDB token
     */
    private fun openWebView(token: String) {
        startActivity(Intent(this, LoginWebViewActivity::class.java).apply {
            putExtra(EXTRA_TOKEN, token)
        })
    }

    /**
     * Opens the show list activity
     */
    private fun openShowList() {
        startActivity(Intent(this, ShowListActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
    }
}