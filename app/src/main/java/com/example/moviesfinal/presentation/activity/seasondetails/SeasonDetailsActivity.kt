package com.example.moviesfinal.presentation.activity.seasondetails

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.example.domain.model.DataResource
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_ID
import com.example.moviesfinal.framework.constant.EXTRA_SEASON_NUMBER
import com.example.moviesfinal.framework.constant.EXTRA_SEASON_POSTER
import com.example.moviesfinal.framework.extension.showToast
import com.example.moviesfinal.presentation.composable.ErrorScreen
import com.example.moviesfinal.presentation.composable.LoadingScreen
import org.koin.androidx.viewmodel.ext.android.viewModel

class SeasonDetailsActivity : ComponentActivity() {

    val viewModel: SeasonDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Get the show id, season number and the show poster
        val showId = intent.getIntExtra(EXTRA_ID, -1)
        val seasonNumber = intent.getIntExtra(EXTRA_SEASON_NUMBER, -1)
        val poster = intent.getStringExtra(EXTRA_SEASON_POSTER) ?: ""

        // Finish the activity if the ID or the season are invalid
        if (showId == -1) {
            this.showToast(getString(R.string.Wrong_Show_ID))
            finish()
        }

        if (seasonNumber == -1) {
            this.showToast(getString(R.string.Wrong_Show_Season))
            finish()
        }

        setContent {
            // Observe the details live data as a state
            val detailsState by viewModel.detailsState.observeAsState()

            // Show screens based on the state
            when (detailsState?.status) {
                DataResource.Status.ERROR -> ErrorScreen {
                    viewModel.loadDetails(
                        showId,
                        seasonNumber
                    )
                }
                DataResource.Status.SUCCESS -> SeasonDetailsLayout(
                    details = detailsState?.data,
                    poster = poster,
                    onBack = { finish() }
                )
                else -> LoadingScreen()
            }
        }

        // Load the details
        viewModel.loadDetails(
            showId,
            seasonNumber
        )
    }
}