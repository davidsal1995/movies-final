package com.example.moviesfinal.presentation.composable

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

/**
 * Creates a scaffold for activities that
 * requires a snack bar
 * @param content is the children view
 * @param snackBarHost is the snack bar host state
 */
@Composable
fun SnackSlot(
    snackBarHost: SnackbarHostState,
    content: @Composable () -> Unit
) {
    Scaffold(bottomBar = {
        SnackbarHost(
            hostState = snackBarHost,
            snackbar = {
                Snackbar(
                    modifier = Modifier
                        .padding(16.dp)
                ) {
                    Text(text = snackBarHost.currentSnackbarData?.message ?: "")
                }
            }
        )
    }) {
        content()
    }
}