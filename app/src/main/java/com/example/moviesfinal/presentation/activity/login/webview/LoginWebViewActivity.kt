package com.example.moviesfinal.presentation.activity.login.webview

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.lifecycleScope
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Session
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_TOKEN
import com.example.moviesfinal.framework.extension.getSharedPreferencesInstance
import com.example.moviesfinal.framework.extension.setSessionId
import com.example.moviesfinal.framework.helper.HandledData
import com.example.moviesfinal.presentation.activity.showlist.ShowListActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginWebViewActivity : ComponentActivity() {

    private val viewModel: LoginWebViewModel by viewModel()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token: String = intent.getStringExtra(EXTRA_TOKEN) ?: ""
        sharedPreferences = this.getSharedPreferencesInstance()

        setContent {
            // Snack bar host state
            val snackBarHost = remember { mutableStateOf(SnackbarHostState()) }
            // Observe the session live data as an state
            val sessionStatus by viewModel.sessionStatus.observeAsState()

            // Finish the activity while showing a snack bar with the error message
            val finishActivity = { message: String ->
                lifecycleScope.launch {
                    snackBarHost.value.showSnackbar(message)
                    delay(2000)
                    finish()
                }
            }

            // Call the function on every session event
            sessionStatus?.let { sessionRequested(it) { message -> finishActivity(message) } }

            // Run once after the composable mounts
            LaunchedEffect(Unit) { if (token.isEmpty()) finishActivity(getString(R.string.Invalid_Token)) }

            // Activity layout
            LoginWebViewLayout(
                snackBarHost = snackBarHost.value,
                token = token,
                context = LocalContext.current,
                onChange = { status ->
                    if (status) viewModel.requestSession(token)
                    else finishActivity(getString(R.string.Check_Connection))
                })
        }
    }

    /**
     * Handles the session event emitted by the view model
     * @param fail is the callback for fail
     * @param handledSession is the handled session emitted
     */
    private fun sessionRequested(
        handledSession: HandledData<DataResource<Session>>,
        fail: (String) -> Unit
    ) {
        // If has not been handled yet
        if (!handledSession.isHandled()) {
            handledSession.setHandled()

            val session = handledSession.data

            // Fail with an internet error
            if (session.status == DataResource.Status.ERROR)
                fail(getString(R.string.Check_Connection))
            else {
                val successSession = session.data?.success ?: false
                val idSession = session.data?.session_id ?: ""

                // Fail with an invalid session
                if (!successSession || idSession.isEmpty()) {
                    fail(getString(R.string.Invalid_Session))
                } else {
                    // Succeed while opening the show list activity
                    if (sharedPreferences.setSessionId(idSession)) openShowList()
                    else fail(getString(R.string.Invalid_Session))
                }
            }
        }
    }

    /**
     * Opens the show list activity
     */
    private fun openShowList() {
        startActivity(Intent(this, ShowListActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
    }
}