package com.example.moviesfinal.presentation.theme

import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

/**
 * File for storing Compose Gradients
 */
val PurpleBlueGradient = Brush.verticalGradient(
    colors = listOf(
        LightBlue,
        Purple
    )
)

val BlackWhiteGradient = Brush.verticalGradient(
    colors = listOf(
        Color.White.copy(0.1f),
        Color.Black
    )
)