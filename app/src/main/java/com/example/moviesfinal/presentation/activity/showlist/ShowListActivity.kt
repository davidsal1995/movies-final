package com.example.moviesfinal.presentation.activity.showlist

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import com.example.domain.model.DataResource
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_ID
import com.example.moviesfinal.framework.extension.showToast
import com.example.moviesfinal.framework.helper.ShowTag
import com.example.moviesfinal.framework.helper.isScrolledToTheEnd
import com.example.moviesfinal.presentation.activity.profile.ProfileActivity
import com.example.moviesfinal.presentation.activity.showdetails.ShowDetailsActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShowListActivity : ComponentActivity() {

    private val viewModel: ShowListViewModel by viewModel()

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initial tag for the selector
        val startingTag = ShowTag.TopRated

        setContent {
            // Coroutine scope for the current composable
            val coroutineScope = rememberCoroutineScope()
            // Status of the live data
            val currentStatus = remember { mutableStateOf(DataResource.Status.LOADING) }
            // Current active tag
            val activeTag = remember { mutableStateOf(startingTag) }
            // For keeping track of the refresh layout
            val isRefreshing = remember { mutableStateOf(false) }
            // Lazy list state
            val listState = rememberLazyListState()
            // Observe the show list live data as a state
            val showListEvent by viewModel.showList.observeAsState()

            // If the list is at the bottom, try to load more
            if (listState.isScrolledToTheEnd()) {
                if (currentStatus.value == DataResource.Status.SUCCESS) {
                    viewModel.getNextPage(activeTag.value)
                }
            }

            // Handle the received live data
            showListEvent?.let {
                if (!it.isHandled()) {
                    it.setHandled()

                    currentStatus.value = it.data.status

                    if (it.data.status == DataResource.Status.ERROR) this.showToast(getString(R.string.Pull_Retry))

                    if (it.data.status == DataResource.Status.LOADING) isRefreshing.value = true
                    else coroutineScope.launch {
                        delay(500)
                        isRefreshing.value = false
                    }

                }
            }

            // On tag change, call the view model
            activeTag.value.let { viewModel.changeTag(it) }

            // Activity layout
            ShowListLayout(
                loading = currentStatus.value == DataResource.Status.LOADING_MORE,
                onProfileClick = { openProfile() }
            ) {
                TagChipSelector(activeTag) { activeTag.value = it }

                ShowListPaging(
                    showList = showListEvent?.data?.data ?: listOf(),
                    listState = listState,
                    isRefreshing = isRefreshing.value,
                    onRefresh = { viewModel.refresh(activeTag.value) },
                    onShowClick = { openShowDetails(it) }
                )
            }
        }

        // Get the first page
        viewModel.getNextPage(startingTag)
    }

    /**
     * Opens the show details activity
     * @param showID is the show id
     */
    private fun openShowDetails(showID: Int) {
        startActivity(Intent(this, ShowDetailsActivity::class.java).apply {
            putExtra(EXTRA_ID, showID)
        })
    }

    /**
     * Opens the profile activity
     */
    private fun openProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
    }
}