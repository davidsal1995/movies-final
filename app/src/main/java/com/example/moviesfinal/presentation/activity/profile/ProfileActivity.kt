package com.example.moviesfinal.presentation.activity.profile

import android.content.Intent
import android.os.Bundle
import android.webkit.CookieManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.example.domain.model.DataResource
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_ID
import com.example.moviesfinal.framework.extension.getSessionId
import com.example.moviesfinal.framework.extension.getSharedPreferencesInstance
import com.example.moviesfinal.framework.extension.setSessionId
import com.example.moviesfinal.framework.extension.showToast
import com.example.moviesfinal.presentation.activity.showdetails.ShowDetailsActivity
import com.example.moviesfinal.presentation.activity.splash.SplashActivity
import com.example.moviesfinal.presentation.composable.ErrorScreen
import com.example.moviesfinal.presentation.composable.LoadingScreen
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileActivity : ComponentActivity() {

    val viewModel: ProfileViewModel by viewModel()
    private var session: String = ""
    private var favoritesInitialized = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        session = this.getSharedPreferencesInstance().getSessionId()

        setContent {
            val accountState by viewModel.profileState.observeAsState()
            val favoriteState by viewModel.favoritesState.observeAsState()
            val openDialog = remember { mutableStateOf(false) }

            /**
             * Activity layout
             */
            when (accountState?.status) {
                DataResource.Status.SUCCESS -> ProfileLayout(
                    profile = accountState?.data,
                    favorites = favoriteState,
                    onShowClick = { id -> openShowDetails(id) },
                    onBackClick = { finish() },
                    openDialog = openDialog.value,
                    logOutCallBack = { openDialog.value = true },
                    dialogCallBack = { leave ->
                        if (!leave) openDialog.value = false
                        else viewModel.logOut(session) { success ->
                            openDialog.value = success
                            logOut(success)
                        }
                    }
                )
                DataResource.Status.ERROR -> ErrorScreen { viewModel.loadProfile(session) }
                else -> LoadingScreen()
            }
        }
    }

    /**
     * Load the profile data
     */
    override fun onResume() {
        super.onResume()
        if (!favoritesInitialized) favoritesInitialized = true
        viewModel.loadProfile(session, favoritesInitialized)
    }

    /**
     * Open the show details activity
     * @param showId is the show ID
     */
    private fun openShowDetails(showId: Int) {
        startActivity(Intent(this, ShowDetailsActivity::class.java).apply {
            putExtra(EXTRA_ID, showId)
        })
    }

    /**
     * Deletes the session and goes back to the splash screen
     */
    private fun logOut(success: Boolean) {
        if (success) {
            // Delete the web cookies
            CookieManager.getInstance().removeAllCookies(null)

            // Clean the session ID
            this.getSharedPreferencesInstance().setSessionId("")

            // Open the splash screen while clearing the back stack
            startActivity(Intent(this, SplashActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        } else
            runOnUiThread { this.showToast(getString(R.string.Check_Connection)) }
    }
}