package com.example.moviesfinal.presentation.activity.showlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.show.*
import com.example.domain.model.DataResource
import com.example.domain.model.show.ShowEntry
import com.example.moviesfinal.framework.helper.HandledData
import com.example.moviesfinal.framework.helper.ShowTag
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * View model for the show list activity
 * @param getAiringToday use case for getting shows airing today
 * @param getOnTv use case for getting shows on tv
 * @param getPopular use case for getting popular shows
 * @param getTopRated use case for getting top rated shows
 */
class ShowListViewModel(
    val getTopRated: GetTopRatedUseCase,
    val getPopular: GetPopularUseCase,
    val getOnTv: GetOnTvUseCase,
    val getAiringToday: GetAiringTodayUseCase,
    val insertShowList: InsertShowListUseCase,
    val getShowListByTag: GetShowListTagAll
) : ViewModel() {

    // Live data for emitting the list of show entries
    val showList = MutableLiveData<HandledData<DataResource<List<ShowEntry>>>>()

    // Current show list
    private val _showList = mutableListOf<ShowEntry>()

    // Flags for the paging
    private var currentTag = ShowTag.TopRated
    private var currentPage = 0
    private var totalPages = 0
    private var cached = false

    // Reset paging flags
    private fun resetFlags(tag: ShowTag) {
        currentTag = tag
        currentPage = 0
        totalPages = 0
        _showList.clear()
    }

    /**
     * Get the next page of shows
     * @param tag is the show tag
     */
    fun getNextPage(tag: ShowTag) {

        // Reset flag in case of changing the show tag
        if (currentTag.name != tag.name) resetFlags(tag)

        // Return if we already reached the last page
        if ((currentPage >= totalPages) && (totalPages != 0)) return

        // Emit a loading if its the first page, or loading more if we are retrieving new pages
        showList.value =
            HandledData(
                when (totalPages) {
                    0 -> DataResource.loading()
                    else -> DataResource.loadingMore(_showList)
                }
            )

        // Coroutine for getting the show list page
        viewModelScope.launch(Dispatchers.IO) {
            // Page to retrieve
            val pageIndex = currentPage + 1

            // Use the corresponding use case per tag
            val page = when (tag) {
                ShowTag.TopRated -> getTopRated(pageIndex)
                ShowTag.Popular -> getPopular(pageIndex)
                ShowTag.OnTv -> getOnTv(pageIndex)
                ShowTag.AiringToday -> getAiringToday(pageIndex)
            }

            // Fail without cleaning the current list on the composable
            if (page.status == DataResource.Status.ERROR) {
                cached = true
                showList.postValue(HandledData(DataResource.error("", listOf())))
                delay(250)
                showList.postValue(HandledData(DataResource.error("", getShowListByTag(tag.value))))
            } else {
                // If there's data, update the flags
                page.data?.let {
                    currentPage = it.page
                    totalPages = it.totalPages

                    _showList.addAll(it.results)
                    insertShowList(it.results, it.page, tag.value)

                    if (cached) {
                        cached = false
                        showList.postValue(
                            when (currentPage) {
                                totalPages -> HandledData(DataResource.done(listOf()))
                                else -> HandledData(DataResource.success(listOf()))
                            }
                        )
                        delay(250)
                    }

                    // Emit the new list with the newly added show entries
                    showList.postValue(
                        when (currentPage) {
                            totalPages -> HandledData(DataResource.done(_showList))
                            else -> HandledData(DataResource.success(_showList))
                        }
                    )
                }
            }

        }
    }

    /**
     * Change the current tag and retrieve the new show entries
     * @param tag is the show tag
     */
    fun changeTag(tag: ShowTag) {
        if (tag != currentTag) {
            getNextPage(tag)
        }
    }

    /**
     * Refresh the current list
     * @param tag is the show tag
     */
    fun refresh(tag: ShowTag) {
        resetFlags(tag)
        getNextPage(tag)
    }
}