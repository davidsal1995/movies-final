package com.example.moviesfinal.presentation.activity.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.lifecycleScope
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.activity.login.activity.LoginActivity
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            AppThemeWrapper(ActivityType.Splash) {
                SplashScreen()
            }
        }

        // Waits 2.5 seconds before launching the login activity
        lifecycleScope.launch {
            delay(2500)
            openLoginActivity()
        }
    }

    /**
     * Opens the login activity
     */
    private fun openLoginActivity() = startActivity(
        Intent(
            this, LoginActivity::
            class.java
        )
    )
}