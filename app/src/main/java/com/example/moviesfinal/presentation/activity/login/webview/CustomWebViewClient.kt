package com.example.moviesfinal.presentation.activity.login.webview

import android.content.Context
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.moviesfinal.framework.constant.REDIRECT_ID
import com.example.moviesfinal.framework.helper.detectConnection

/**
 * Custom web client for the web view
 * @param context is the activity context
 * @param onChange is the onChange callback
 */
class CustomWebViewClient(
    private val context: Context,
    val onChange: (Boolean) -> Unit
) : WebViewClient() {
    /**
     * Stops the web view from loading the new url, true is for avoiding the load
     */
    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        return when {
            // If there is no connection, fail
            !detectConnection(context) -> {
                onChange(false)
                true
            }
            else -> {
                when {
                    // If the URL contains the redirect URL, succeed
                    url?.contains(REDIRECT_ID.replace(" ", "%20")) == true -> {
                        onChange(true)
                        true
                    }
                    // Otherwise fail
                    else -> false
                }
            }
        }
    }

    /**
     * If an error occurs, call onChange with false
     */
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        onChange(false)
    }
}