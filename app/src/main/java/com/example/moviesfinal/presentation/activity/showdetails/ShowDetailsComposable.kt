package com.example.moviesfinal.presentation.activity.showdetails

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Bookmark
import androidx.compose.material.icons.filled.BookmarkBorder
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.domain.model.DataResource
import com.example.domain.model.show.ShowDetails
import com.example.domain.model.show.ShowFavoriteState
import com.example.domain.model.show.ShowSeason
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.POSTER_URL
import com.example.moviesfinal.framework.helper.isScrolledTop
import com.example.moviesfinal.framework.helper.isTablet
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.BackTopBar
import com.example.moviesfinal.presentation.composable.BackTopBarIcon
import com.example.moviesfinal.presentation.composable.RatingStars
import com.example.moviesfinal.presentation.theme.*
import com.skydoves.landscapist.glide.GlideImage

/**
 * Favorite bookmark button
 * @param favorite is the state of marked or not as favorite
 * @param onClick is the callback when the favorite is clicked
 */
@Composable
fun FavoriteButton(favorite: DataResource<ShowFavoriteState>?, onClick: () -> Unit) {
    favorite?.let {
        when (it.status) {
            DataResource.Status.ERROR,
            DataResource.Status.SUCCESS -> Icon(
                if (it.data?.favorite == true) Icons.Filled.Bookmark else Icons.Filled.BookmarkBorder,
                stringResource(R.string.Back),
                Modifier
                    .clip(CircleShape)
                    .size(28.dp)
                    .clickable(onClick = onClick),
                CarrotOrange
            )
            else -> CircularProgressIndicator(
                modifier = Modifier
                    .size(28.dp),
                color = CarrotOrange,
                strokeWidth = 3.dp
            )
        }
    }
}

/**
 * View with the image background, a nice return button,
 * texts with the original and english name and at the end, a nice
 * rating stars composable
 * @param onBack is the callback when the back icon is pressed
 * @param backDrop is the background image
 * @param name is the english name
 * @param originalName is the original language name
 * @param poster is the poster image
 * @param voteAverage is the rating
 */
@Composable
fun DetailsHeader(
    backDrop: String,
    poster: String,
    originalName: String,
    name: String,
    voteAverage: Double,
    onBack: () -> Unit,
    favorite: DataResource<ShowFavoriteState>?,
    onFavoriteClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(if (LocalContext.current.isTablet()) 500.dp else 270.dp)
    ) {
        GlideImage(
            imageModel = "$POSTER_URL${
                with(backDrop) { if (isEmpty()) poster else this }
            }",
            contentScale = ContentScale.Crop,
            circularRevealedEnabled = true,
            placeHolder = ImageBitmap.imageResource(R.drawable.movie_big_placeholder),
            alignment = Alignment.Center,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(BlackWhiteGradient)
        ) {
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxHeight()
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Bottom
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(0.dp)
                        .weight(1f)
                ) { BackTopBarIcon(onBack = onBack) }

                Text(
                    text = originalName,
                    color = WhiteText,
                    style = MaterialTheme.typography.caption,
                    fontSize = 14.sp,
                    maxLines = 1
                )

                Text(
                    text = name,
                    color = WhiteText,
                    style = MaterialTheme.typography.h4,
                    fontSize = 36.sp,
                    maxLines = 3
                )

                Row {
                    RatingStars(
                        rating = voteAverage / 2,
                        size = 25.dp,
                        color = LightCyanStar,
                        label = false
                    )

                    Spacer(
                        modifier = Modifier
                            .width(0.dp)
                            .weight(1f)
                    )

                    FavoriteButton(favorite, onFavoriteClick)

                }
            }
        }
    }
}

/**
 * Summary block with a nice title and the overview
 * @param overview is the show overview
 */
@Composable
fun SummaryBlock(overview: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Text(
            text = stringResource(R.string.Summary),
            color = MaterialTheme.colors.primary,
            style = MaterialTheme.typography.h6,
            fontSize = 22.sp,
            maxLines = 1,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        Text(
            text = overview,
            color = DarkGrayText,
            style = MaterialTheme.typography.body2,
            fontSize = 16.sp
        )
    }
}

/**
 * Simple season item to show the season name, a poster,
 * number of episodes and a overview (if exists)
 * @param onSeasonPress callback when the item is pressed
 * @param season is the show season
 * @param showPoster is the show poster in case the season doesn't have one
 */
@Composable
fun SeasonEntry(season: ShowSeason, showPoster: String, onSeasonPress: (Int, String) -> Unit) {
    val poster = with(season.posterPath) { if (isEmpty()) showPoster else this }
    Row(
        modifier = Modifier
            .height(180.dp)
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .shadow(4.dp, RoundedCornerShape(8.dp))
            .clip(RoundedCornerShape(8.dp))
            .fillMaxWidth()
            .background(PureWhiteBackGround)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(
                    bounded = true,
                    color = MaterialTheme.colors.primary
                ),
                onClick = {
                    if (season.episodeCount > 0) onSeasonPress(
                        season.seasonNumber,
                        poster
                    )
                }
            )
    ) {
        GlideImage(
            imageModel = "$POSTER_URL${poster}",
            contentScale = ContentScale.Crop,
            circularRevealedEnabled = true,
            placeHolder = ImageBitmap.imageResource(R.drawable.entry_placeholder),
            alignment = Alignment.Center,
            modifier = Modifier
                .width(110.dp)
                .fillMaxHeight(),
        )
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = season.name,
                color = DarkGrayText,
                style = MaterialTheme.typography.h6,
                fontSize = 22.sp,
                maxLines = 1,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            Text(
                text = "${season.episodeCount} Episode${if (season.episodeCount > 1) "s" else ""}",
                color = MaterialTheme.colors.primary,
                fontSize = 14.sp,
                maxLines = 1,
                modifier = Modifier.padding(bottom = 8.dp),
                fontWeight = FontWeight.SemiBold
            )
            Text(
                text = with(season.overview) { if (this.isEmpty()) "No overview available. " else this },
                color = DarkGrayText,
                style = MaterialTheme.typography.body2,
                fontSize = 16.sp,
                maxLines = 3,
                modifier = Modifier.padding(bottom = 8.dp)
            )
        }
    }
}

/**
 * Show details layout with a top bar that auto hides, a header, an overview and
 * the season list
 * @param details is the show details
 * @param onBack is the callback when the back icon button is pressed
 * @param onSeasonPress is the callback when the season is pressed
 */
@Composable
fun ShowDetailsLayout(
    details: ShowDetails?,
    onBack: () -> Unit,
    onSeasonPress: (Int, String) -> Unit,
    favorite: DataResource<ShowFavoriteState>?,
    onFavoriteClick: () -> Unit
) {
    details?.let {
        AppThemeWrapper {
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
            ) {
                // Keeps the state of the lazy list
                val listState = rememberLazyListState()

                LazyColumn(state = listState) {
                    item {
                        DetailsHeader(
                            it.backDropPath,
                            it.posterPath,
                            it.originalName,
                            it.name,
                            it.voteAverage,
                            onBack,
                            favorite,
                            onFavoriteClick
                        )
                    }
                    item { SummaryBlock(it.overview) }
                    items(details.seasons) {
                        SeasonEntry(
                            it,
                            details.posterPath,
                            onSeasonPress
                        )
                    }
                    item { Spacer(Modifier.padding(bottom = 12.dp)) }
                }

                // Show the top bar if the list is at the top
                if (!listState.isScrolledTop()) BackTopBar(details.name, onBack)
            }
        }
    }
}