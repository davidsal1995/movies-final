package com.example.moviesfinal.presentation.activity.splash

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.composable.AppIcon
import com.example.moviesfinal.presentation.composable.AppTextTitle
import com.example.moviesfinal.presentation.theme.PurpleBlueGradient

/**
 * Splash screen that contains the app icon
 * and the app title in the center
 */
@Composable
fun SplashScreen() {
    Row(
        modifier = Modifier
            .background(
                brush = PurpleBlueGradient
            )
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        AppIcon(splash = true)
        AppTextTitle(ActivityType.Splash)
    }
}