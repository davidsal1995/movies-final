package com.example.moviesfinal.presentation.activity.login.webview

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.webkit.WebView
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidView
import com.example.data.util.constant.AUTH_URL
import com.example.moviesfinal.framework.constant.REDIRECT_ID
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.SnackSlot

/**
 * Simple Web View that shows TMDB login screen
 * @param context is the activity context
 * @param onChange is the url change callback
 * @param urlToRender is the URL to render
 */
@SuppressLint("SetJavaScriptEnabled")
@Composable
fun WebLoginView(
    context: Context,
    urlToRender: String,
    onChange: (Boolean) -> Unit
) {
    AndroidView(factory = {
        WebView(it).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            webViewClient = CustomWebViewClient(context, onChange)
            settings.javaScriptEnabled = true
        }
    }, update = {
        it.loadUrl(urlToRender)
    })
}

/**
 * Activity layout with a simple web view
 * @param context is the activity context
 * @param onChange is the status change callback
 * @param snackBarHost is the snack bar state
 * @param token is a TMDB token
 */
@Composable
fun LoginWebViewLayout(
    context: Context,
    snackBarHost: SnackbarHostState,
    token: String,
    onChange: (Boolean) -> Unit
) {
    AppThemeWrapper(ActivityType.WebView) {
        SnackSlot(snackBarHost) {
            WebLoginView(
                urlToRender = "$AUTH_URL$token?redirect_to=$REDIRECT_ID",
                context = context,
                onChange = onChange
            )
        }
    }
}
