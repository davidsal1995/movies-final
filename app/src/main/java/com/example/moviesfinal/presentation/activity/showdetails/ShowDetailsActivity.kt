package com.example.moviesfinal.presentation.activity.showdetails

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.example.domain.model.DataResource
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.EXTRA_ID
import com.example.moviesfinal.framework.constant.EXTRA_SEASON_NUMBER
import com.example.moviesfinal.framework.constant.EXTRA_SEASON_POSTER
import com.example.moviesfinal.framework.extension.getSessionId
import com.example.moviesfinal.framework.extension.getSharedPreferencesInstance
import com.example.moviesfinal.framework.extension.showToast
import com.example.moviesfinal.presentation.activity.seasondetails.SeasonDetailsActivity
import com.example.moviesfinal.presentation.composable.ErrorScreen
import com.example.moviesfinal.presentation.composable.LoadingScreen
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShowDetailsActivity : ComponentActivity() {

    val viewModel: ShowDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Show ID
        val showId = intent.getIntExtra(EXTRA_ID, -1)

        // Session ID
        val session = this.getSharedPreferencesInstance().getSessionId()

        // If the ID is invalid, finish the activity
        if (showId == -1) {
            this.showToast(getString(R.string.Wrong_Show_ID))
            finish()
        }

        setContent {
            // Observe the details live data as a state
            val detailsState by viewModel.detailsState.observeAsState()
            val favoriteState by viewModel.favoriteState.observeAsState()

            // Show different screens depending on the emitted details state
            when (detailsState?.status) {
                DataResource.Status.ERROR -> ErrorScreen {
                    viewModel.loadDetails(showId)
                    viewModel.loadFavorite(showId, session)
                }
                DataResource.Status.SUCCESS -> ShowDetailsLayout(
                    details = detailsState?.data,
                    onBack = { finish() },
                    onSeasonPress = { id, poster -> openSeasonDetails(id, showId, poster) },
                    favorite = favoriteState,
                    onFavoriteClick = {
                        favoriteState?.data?.let {
                            viewModel.setFavorite(
                                marked = !it.favorite,
                                session = session,
                                show = showId,
                                onError = { showErrorToast() }
                            )
                        }
                    }
                )
                else -> LoadingScreen()
            }
        }

        // Load details
        viewModel.loadDetails(showId)

        // Load favorite
        viewModel.loadFavorite(showId, session)
    }

    private fun showErrorToast() {
        runOnUiThread {
            this.showToast(getString(R.string.No_Internet))
        }
    }

    /**
     * Open season details activity
     * @param poster is the poster image
     * @param seasonNumber is the season number
     * @param showId is the show id
     */
    private fun openSeasonDetails(seasonNumber: Int, showId: Int, poster: String) {
        startActivity(Intent(this, SeasonDetailsActivity::class.java).apply {
            putExtra(EXTRA_ID, showId)
            putExtra(EXTRA_SEASON_NUMBER, seasonNumber)
            putExtra(EXTRA_SEASON_POSTER, poster)
        })
    }
}

