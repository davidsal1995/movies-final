package com.example.moviesfinal.presentation.activity.showdetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.auth.GetAccountUseCase
import com.example.domain.interactor.show.GetShowDetailsUseCase
import com.example.domain.interactor.show.GetShowFavoriteStateUseCase
import com.example.domain.interactor.show.SetShowFavoriteStateUseCase
import com.example.domain.model.DataResource
import com.example.domain.model.account.FavoriteBody
import com.example.domain.model.show.ShowDetails
import com.example.domain.model.show.ShowFavoriteState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model for the show details activity
 * @param getDetails is the use case for getting the show details
 * @param getFavorite is the use case for getting the show favorite state
 * @param getAccount is the use case for getting the account details
 */
class ShowDetailsViewModel(
    val getDetails: GetShowDetailsUseCase,
    val getFavorite: GetShowFavoriteStateUseCase,
    val getAccount: GetAccountUseCase,
    val setShowFavorite: SetShowFavoriteStateUseCase
) : ViewModel() {
    // Live data for emitting show details events
    val detailsState = MutableLiveData<DataResource<ShowDetails>>()

    // Live data for emitting favorite events
    val favoriteState = MutableLiveData<DataResource<ShowFavoriteState>>()

    // Account ID
    private var accountId = -1

    /**
     * Retrieves a show details
     * @param id is the show id
     */
    fun loadDetails(id: Int) {
        // Emit a loading state
        detailsState.value = DataResource.loading()

        // Fail with an error or emit a successful details event
        viewModelScope.launch(Dispatchers.IO) {
            with(getDetails(id)) {
                when (this.status) {
                    DataResource.Status.ERROR -> detailsState.postValue(DataResource.error())
                    else -> detailsState.postValue(this)
                }
            }
        }
    }

    /**
     * Gets the favorite state for the show
     * @param id is the show ID
     * @param session is the TMDB session
     */
    fun loadFavorite(id: Int, session: String) {
        // Emit a loading state
        favoriteState.value = DataResource.loading()

        // Emit the event result
        viewModelScope.launch(Dispatchers.IO) {
            with(getFavorite(id, session)) {
                favoriteState.postValue(this)
            }
        }
    }

    /**
     * Send the previous favorite state
     * @param state is the previous state
     */
    private fun onErrorResetFavorite(state: Boolean, onError: () -> Unit) {
        favoriteState.postValue(
            DataResource.error(
                data = ShowFavoriteState(!state)
            )
        )
        onError()
    }

    /**
     * Sets the show favorite state
     * @param marked is the favorite state
     * @param session is the TMDB session
     * @param show is the show ID
     */
    fun setFavorite(marked: Boolean, session: String, show: Int, onError: () -> Unit) {
        // Emit a loading state
        favoriteState.value = DataResource.loading()

        // Start the POST process
        viewModelScope.launch(Dispatchers.IO) {
            // Get the account ID
            if (accountId == -1) with(getAccount(session)) {
                if (status == DataResource.Status.SUCCESS) accountId = data?.id ?: -1
                else onErrorResetFavorite(marked, onError)
            }

            if (accountId != -1) with(
                setShowFavorite(
                    accountId,
                    session,
                    FavoriteBody(show, marked)
                )
            ) {
                if (status == DataResource.Status.ERROR) onErrorResetFavorite(marked, onError)
                else favoriteState.postValue(
                    DataResource(
                        status,
                        ShowFavoriteState(marked),
                        message
                    )
                )
            }
        }
    }
}