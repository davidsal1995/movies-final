package com.example.moviesfinal.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

/**
 * File for storing Compose Themes
 */
private val LightColorPalette = lightColors(
    primary = Purple,
    primaryVariant = Purple700,
    secondary = LightBlue,
    background = WhiteBackground,
)

// Use the same color palette
private val DarkColorPalette = LightColorPalette

// Wrapper for the app theme
@Composable
fun MoviesFinalTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}