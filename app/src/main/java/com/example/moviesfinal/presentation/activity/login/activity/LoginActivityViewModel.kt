package com.example.moviesfinal.presentation.activity.login.activity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.auth.GetTokenUseCase
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Token
import com.example.moviesfinal.framework.helper.HandledData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model for the login process
 * @param getToken is the get token use case
 */
class LoginActivityViewModel(
    val getToken: GetTokenUseCase
) : ViewModel() {

    // Live data for the token
    val tokenStatus = MutableLiveData<HandledData<DataResource<Token>>>()

    // Requests a new token using the API
    fun requestToken() {
        viewModelScope.launch(Dispatchers.IO) { tokenStatus.postValue(HandledData(getToken())) }
    }
}