package com.example.moviesfinal.presentation.activity.profile

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.domain.model.account.Account
import com.example.domain.model.show.ShowDetails
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.constant.POSTER_URL
import com.example.moviesfinal.framework.extension.toShowEntry
import com.example.moviesfinal.presentation.activity.showlist.ShowCell
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.BackTopBar
import com.example.moviesfinal.presentation.theme.*
import com.skydoves.landscapist.glide.GlideImage

/**
 * Log out button
 * @param onClick is the callback for the button onclick
 */
@Composable
fun LogOutButton(onClick: () -> Unit = {}) {
    Button(
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .width(340.dp),
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            backgroundColor = MaterialTheme.colors.primary
        )

    ) {
        Text(
            modifier = Modifier
                .padding(vertical = 7.dp),
            text = stringResource(R.string.Log_Out),
            fontSize = 15.sp,
            color = WhiteBackground,
        )
    }
}

/**
 * Shows the avatar, name and username of the account
 * @param account is the account details
 */
@Composable
fun UserData(account: Account) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .size(135.dp)
                .background(color = Purple.copy(0.15f)),
            contentAlignment = Alignment.Center
        ) {
            Box(
                modifier = Modifier
                    .clip(CircleShape)
                    .size(100.dp)
                    .background(color = WhiteBackground),
                contentAlignment = Alignment.Center
            ) {
                GlideImage(
                    imageModel = "$POSTER_URL${account.avatar.tmdb.avatarPath}",
                    contentScale = ContentScale.Crop,
                    circularRevealedEnabled = true,
                    placeHolder = Icons.Filled.AccountCircle,
                    alignment = Alignment.Center,
                    modifier = Modifier
                        .size(90.dp)
                        .clip(CircleShape)
                )
            }
            Box(
                modifier = Modifier
                    .padding(top = 50.dp, start = 50.dp)
                    .size(40.dp)
                    .clip(CircleShape)
                    .background(MaterialTheme.colors.primary)
                    .clickable { },
                contentAlignment = Alignment.Center
            ) {
                Icon(
                    Icons.Filled.Edit,
                    stringResource(R.string.Back),
                    Modifier
                        .clip(CircleShape)
                        .size(24.dp),
                    WhiteText
                )
            }
        }
        Text(
            text = account.name,
            color = DarkGrayText,
            style = MaterialTheme.typography.subtitle1,
            fontSize = 22.sp,
            modifier = Modifier.padding(top = 6.dp, bottom = 4.dp),
            fontWeight = FontWeight.SemiBold
        )
        Text(
            text = "@${account.username}",
            color = SubtleGrayText,
            style = MaterialTheme.typography.caption,
            fontSize = 17.sp,
        )
    }
}

/**
 * List row of shows marked as favorites
 * @param onShowClick is the callback for the item click
 * @param shows is the list of favorite shows
 */
@Composable
fun Favorites(shows: List<ShowDetails>, onShowClick: (Int) -> Unit) {
    if (shows.isNotEmpty()) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = stringResource(R.string.My_Favorites),
                color = DarkGrayText,
                style = MaterialTheme.typography.h6,
                fontSize = 22.sp,
                modifier = Modifier.padding(start = 16.dp, bottom = 24.dp)
            )
            LazyRow {
                items(shows) {
                    Box(modifier = Modifier.width(180.dp)) {
                        ShowCell(it.toShowEntry(), onShowClick)
                    }
                }
                item { Spacer(modifier = Modifier.width(16.dp)) }
            }
        }
    }
}

/**
 * Dialog for asking between stay and leave
 * @param onClick is the button value click
 */
@Composable
fun LogOutDialog(onClick: (Boolean) -> Unit) {
    val loading = remember { mutableStateOf(false) }
    AlertDialog(
        modifier = Modifier
            .wrapContentHeight()
            .clip(RoundedCornerShape(8.dp)),
        onDismissRequest = { onClick(false) },
        text = {
            if (!loading.value) Text(
                text = stringResource(R.string.Sure_Want_Leave),
                color = Color.Black.copy(0.6f),
                fontSize = 16.sp
            )
            else Box(
                modifier = Modifier
                    .padding(0.dp)
                    .fillMaxWidth()
                    .padding(top = 16.dp),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator()
            }
        },
        confirmButton = {
            if (!loading.value) Text(
                text = stringResource(R.string.Stay),
                fontSize = 16.sp,
                color = MaterialTheme.colors.primary,
                modifier = Modifier
                    .padding(0.dp)
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(
                            bounded = true,
                            color = MaterialTheme.colors.primary
                        ),
                        onClick = { onClick(false) }
                    )
                    .padding(12.dp),
                style = MaterialTheme.typography.button
            )
        },
        dismissButton = {
            if (!loading.value) Text(
                text = stringResource(R.string.Leave),
                fontSize = 16.sp,
                color = ErrorRed,
                modifier = Modifier
                    .padding(0.dp)
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(
                            bounded = true,
                            color = MaterialTheme.colors.primary
                        ),
                        onClick = {
                            loading.value = true
                            onClick(true)
                        }
                    )
                    .padding(12.dp),
                style = MaterialTheme.typography.button
            )
        }
    )
}

/**
 * Activity Layout
 * @param dialogCallBack is the callback when an dialog action is clicked
 * @param logOutCallBack is the callback when the log out button is clicked
 * @param onBackClick is the callback when the back top bar icon is clicked
 * @param onShowClick is the callback when a favorite show item is clicked
 * @param openDialog is the dialog open/closed state
 * @param profile is the account and favorites data
 */
@Composable
fun ProfileLayout(
    profile: Account?,
    favorites: List<ShowDetails>?,
    onBackClick: () -> Unit,
    onShowClick: (Int) -> Unit,
    openDialog: Boolean,
    dialogCallBack: (Boolean) -> Unit,
    logOutCallBack: () -> Unit
) {
    AppThemeWrapper {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            BackTopBar("Profile", onBackClick)
            profile?.let { UserData(it) }
            favorites?.let { Favorites(it, onShowClick) }
            LogOutButton(logOutCallBack)
        }

        if (openDialog) LogOutDialog(dialogCallBack)
    }
}