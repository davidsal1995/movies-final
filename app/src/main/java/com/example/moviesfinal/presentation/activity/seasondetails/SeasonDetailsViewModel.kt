package com.example.moviesfinal.presentation.activity.seasondetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interactor.show.GetSeasonDetailsUseCase
import com.example.domain.model.DataResource
import com.example.domain.model.show.SeasonDetails
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model for the season details activity
 * @param getDetails is the use case for getting the season details
 */
class SeasonDetailsViewModel(
    val getDetails: GetSeasonDetailsUseCase
) : ViewModel() {
    // Live data for emitting season details events
    val detailsState = MutableLiveData<DataResource<SeasonDetails>>()

    /**
     * Retrieves the season details
     * @param id is the show id
     * @param number is the season number
     */
    fun loadDetails(id: Int, number: Int) {
        // Emit a loading state
        detailsState.value = DataResource.loading()

        // Fail with an error or emit a successful event
        viewModelScope.launch(Dispatchers.IO) {
            with(getDetails(id, number)) {
                when (this.status) {
                    DataResource.Status.ERROR -> detailsState.postValue(DataResource.error())
                    else -> detailsState.postValue(this)
                }
            }
        }
    }
}