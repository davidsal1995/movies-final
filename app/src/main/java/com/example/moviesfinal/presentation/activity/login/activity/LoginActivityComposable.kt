package com.example.moviesfinal.presentation.activity.login.activity

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.moviesfinal.R
import com.example.moviesfinal.framework.helper.ActivityType
import com.example.moviesfinal.presentation.composable.AppIcon
import com.example.moviesfinal.presentation.composable.AppTextTitle
import com.example.moviesfinal.presentation.composable.AppThemeWrapper
import com.example.moviesfinal.presentation.composable.SnackSlot
import com.example.moviesfinal.presentation.theme.DarkGrayText
import com.example.moviesfinal.presentation.theme.WhiteBackground

/**
 * Simple info text
 */
@Composable
fun LoginInfoText() {
    Text(
        text = "Sign in to your account",
        color = DarkGrayText,
        style = MaterialTheme.typography.body1,
        fontSize = 18.sp,
        modifier = Modifier.padding(top = 35.dp, bottom = 50.dp)
    )
}

/**
 * Login button
 * @param loading is a flag for showing a loading spinner instead
 * @param onClick is the onclick callback
 */
@Composable
fun LoginButton(loading: Boolean, onClick: () -> Unit) {
    when {
        loading -> CircularProgressIndicator()
        else -> Button(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .width(340.dp),
            onClick = onClick,
            colors = ButtonDefaults.textButtonColors(
                backgroundColor = MaterialTheme.colors.primary
            )

        ) {
            Text(
                modifier = Modifier
                    .padding(vertical = 7.dp),
                text = stringResource(R.string.Login_Button),
                fontSize = 15.sp,
                color = WhiteBackground,
            )
        }
    }
}

/**
 * Activity layout with the app icon, the app title text,
 * an info text, a login button and a snack bar for showing errors
 */
@Composable
fun LoginActivityLayout(
    loading: Boolean,
    snackBarHost: SnackbarHostState,
    onClick: () -> Unit
) {
    SnackSlot(snackBarHost) {
        AppThemeWrapper(ActivityType.Login) {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                AppIcon()
                AppTextTitle()
                LoginInfoText()
                LoginButton(loading) {
                    onClick()
                }
            }
        }
    }
}