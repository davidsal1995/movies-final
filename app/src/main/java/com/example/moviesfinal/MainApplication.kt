package com.example.moviesfinal

import android.app.Application
import com.example.data.di.databaseModule
import com.example.data.di.networkModule
import com.example.data.di.repositoryModule
import com.example.domain.di.interactionModule
import com.example.moviesfinal.framework.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Koin modules
 */
val appModules = listOf(presentationModule)
val dataModules = listOf(databaseModule, networkModule, repositoryModule)
val domainModules = listOf(interactionModule)

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // Start koin context and modules
        startKoin {
            androidContext(this@MainApplication)
            modules(domainModules + dataModules + appModules)
        }
    }
}

