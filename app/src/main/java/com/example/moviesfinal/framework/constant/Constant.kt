package com.example.moviesfinal.framework.constant

// App ID
const val APP_ID = "com.example.moviesfinal"

// Redirect URL
const val REDIRECT_ID = "go to main screen"

// Shared Preferences related constants
const val SHARED_PREFERENCES_NAME = "Settings"
const val SHARED_PREFERENCE_SESSION_ID = "SessionID"

// Intent extras related constants
const val EXTRA_TOKEN = "$APP_ID.extra_token"
const val EXTRA_ID = "$APP_ID.extra_id"
const val EXTRA_SEASON_NUMBER = "$APP_ID.extra_season_number"
const val EXTRA_SEASON_POSTER = "$APP_ID.extra_season_poster"

// Base URL for poster images
const val POSTER_URL = "https://image.tmdb.org/t/p/original"