package com.example.moviesfinal.framework.extension

import android.content.Context
import android.widget.Toast

/**
 * Creates a toast based on the context
 * @param text is the text to be shown
 * @param long is the duration of the toast
 */
fun Context.showToast(text: String, long: Boolean = false) {
    Toast.makeText(
        this,
        text,
        if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
    ).show()
}