package com.example.moviesfinal.framework.helper

import androidx.compose.foundation.lazy.LazyListState

/**
 * Detect if a lazy list is at the bottom
 */
fun LazyListState.isScrolledToTheEnd() =
    layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1

/**
 * Detect if a lazy list is at the top
 */
fun LazyListState.isScrolledTop() =
    layoutInfo.visibleItemsInfo.firstOrNull()?.index == 0