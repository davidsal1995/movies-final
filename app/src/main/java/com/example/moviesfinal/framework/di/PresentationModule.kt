package com.example.moviesfinal.framework.di

import com.example.moviesfinal.presentation.activity.login.activity.LoginActivityViewModel
import com.example.moviesfinal.presentation.activity.login.webview.LoginWebViewModel
import com.example.moviesfinal.presentation.activity.profile.ProfileViewModel
import com.example.moviesfinal.presentation.activity.seasondetails.SeasonDetailsViewModel
import com.example.moviesfinal.presentation.activity.showdetails.ShowDetailsViewModel
import com.example.moviesfinal.presentation.activity.showlist.ShowListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Koin module for handling the creation of view models
 */
val presentationModule = module {

    /**
     * Depends on: GetTokenUseCase
     * Injected into: LoginActivity
     */
    viewModel { LoginActivityViewModel(get()) }

    /**
     * Depends on: GetSessionUseCase
     * Injected into: LoginWebViewActivity
     */
    viewModel { LoginWebViewModel(get()) }

    /**
     * Depends on: GetTopRatedUseCase, GetPopularUseCase, GetOnTvUseCase, GetAiringTodayUseCase
     * Injected into: ShowListActivity
     */
    viewModel { ShowListViewModel(get(), get(), get(), get(), get(), get()) }

    /**
     * Depends on: GetShowDetailsUseCase, GetFavoriteUseCase,
     * GetAccountUseCase, SetShowFavoriteUseCase
     * Injected into: ShowDetailsActivity
     */
    viewModel { ShowDetailsViewModel(get(), get(), get(), get()) }

    /**
     * Depends on: GetSeasonDetailsUseCase
     * Injected into: SeasonDetailsActivity
     */
    viewModel { SeasonDetailsViewModel(get()) }

    /**
     * Depends on: GetAccountUseCase, GetAccountFavoritesUseCase, DeleteSessionUseCase
     * Injected into: ProfileActivity
     */
    viewModel { ProfileViewModel(get(), get(), get()) }
}