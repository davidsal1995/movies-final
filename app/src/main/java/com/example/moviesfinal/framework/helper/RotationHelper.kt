package com.example.moviesfinal.framework.helper

import android.content.Context
import com.example.moviesfinal.R

fun Context.isTablet() = this.resources.getBoolean(R.bool.isTab)
