package com.example.moviesfinal.framework.helper

/**
 * Enum class for storing the different types of activities in the app
 */
enum class ActivityType {
    Splash,
    Login,
    WebView,
    General
}