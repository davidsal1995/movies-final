package com.example.moviesfinal.framework.helper

/**
 * Enum for storing the available show tags
 */
enum class ShowTag(val value: String) {
    TopRated("Top Rated"),
    Popular("Popular"),
    OnTv("On Tv"),
    AiringToday("Airing Today"),
}