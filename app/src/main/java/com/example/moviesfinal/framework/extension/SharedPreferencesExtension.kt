package com.example.moviesfinal.framework.extension

import android.content.Context
import android.content.SharedPreferences
import com.example.moviesfinal.framework.constant.SHARED_PREFERENCES_NAME
import com.example.moviesfinal.framework.constant.SHARED_PREFERENCE_SESSION_ID

/**
 * Gets the shared preferences using the context
 */
fun Context.getSharedPreferencesInstance(): SharedPreferences = this.getSharedPreferences(
    SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE
)

/**
 * Gets the Session ID stored in the shared preferences
 */
fun SharedPreferences.getSessionId(): String =
    this.getString(SHARED_PREFERENCE_SESSION_ID, "") ?: ""

/**
 * Stores the session ID in the shared preferences
 */
fun SharedPreferences.setSessionId(session: String) = this.edit().putString(
    SHARED_PREFERENCE_SESSION_ID, session
).commit()