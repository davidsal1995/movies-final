package com.example.moviesfinal.framework.extension

import com.example.domain.model.show.ShowDetails
import com.example.domain.model.show.ShowEntry

/**
 * Converts a Show Details into a Show Entry
 */
fun ShowDetails.toShowEntry() = ShowEntry(
    this.id,
    this.posterPath,
    this.name,
    this.voteAverage
)