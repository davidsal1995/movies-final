package com.example.moviesfinal.framework.helper

/**
 * Wrapper for live data events
 * Used to check if the data has already been handled
 */
data class HandledData<out T>(
    val data: T,
    private var handled: Boolean = false
) {
    fun setHandled() {
        handled = true
    }

    fun isHandled() = handled
}