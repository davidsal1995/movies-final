package com.example.data.di

import androidx.room.Room
import com.example.data.database.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    /**
     * Creates a room database instance
     * Injected into: ShowDao
     */
    factory {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "shows")
            .fallbackToDestructiveMigration()
            .build()
    }

    /**
     * Creates a ShowDao singleton
     * Injected into: ShowRepository
     */
    single { get<AppDatabase>().showDao() }
}