package com.example.data.di

import com.example.data.networking.remote.authorization.AuthRemoteDataSource
import com.example.data.networking.remote.authorization.AuthService
import com.example.data.networking.remote.show.ShowRemoteDataSource
import com.example.data.networking.remote.show.ShowService
import com.example.data.util.constant.API_URL
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Koin module for creating network related dependencies
 */
val networkModule = module {

    /**
     * Creates a Gson converter instance
     * Injected into: Retrofit
     */
    factory { GsonConverterFactory.create(GsonBuilder().create()) as Converter.Factory }

    /**
     * Creates a Retrofit instance
     * Injected into: ShowService
     */
    single {
        Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(get())
            .build()
    }

    /**
     * Creates a ShowService instance
     * Injected into: ShowRemoteDataSource
     */
    single { get<Retrofit>().create(AuthService::class.java) }

    /**
     * Creates an AuthService instance
     * Injected into: AuthRemoteDataSource
     */
    single { get<Retrofit>().create(ShowService::class.java) }

    /**
     * Creates an AuthRemoteDataSource
     * Injected into: AuthRepositoryImpl
     */
    factory { AuthRemoteDataSource(get()) }

    /**
     * Creates a ShowRemoteDataSource
     * Injected into: ShowRepositoryImpl
     */
    factory { ShowRemoteDataSource(get()) }
}