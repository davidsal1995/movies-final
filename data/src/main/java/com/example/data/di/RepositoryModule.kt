package com.example.data.di

import com.example.data.repository.AuthRepositoryImpl
import com.example.data.repository.ShowRepositoryImpl
import com.example.domain.repository.AuthRepository
import com.example.domain.repository.ShowRepository
import org.koin.dsl.module

/**
 * Koin module that handles the creation of repository dependencies
 */
val repositoryModule = module {
    /**
     * Creates an AuthRepository instance from an AuthRepositoryImpl
     * Injected into: Auth use cases
     */
    factory<AuthRepository> { AuthRepositoryImpl(get()) }

    /**
     * Creates a ShowRepository instance from a ShowRepositoryImpl
     * Injected into: Show use cases
     */
    factory<ShowRepository> { ShowRepositoryImpl(get(), get()) }
}