package com.example.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Data class to hold the show data for DB manipulation
 */
@Entity(tableName = "shows", indices = [Index(value = ["id", "tag"], unique = true)])
data class ShowEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "poster") val posterPath: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "vote_average") val voteAverage: Double,
    @ColumnInfo(name = "page") val page: Int,
    @ColumnInfo(name = "tag") val tag: String
)