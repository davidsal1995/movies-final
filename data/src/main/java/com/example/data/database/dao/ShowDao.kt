package com.example.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.entity.ShowEntity

/**
 * Data Access Object interface for handling shows related data
 */
@Dao
interface ShowDao {

    /**
     * Inserts the retrieved shows into the local DB
     * @param shows is the list of shows
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(shows: List<ShowEntity>)

    /**
     * Get all shows from the DB with the provided tag
     * @param tag is the show tag
     */
    @Query("SELECT * FROM shows WHERE tag = :tag ORDER BY vote_average DESC")
    suspend fun getShowListTagAll(tag: String): List<ShowEntity>

}