package com.example.data.networking.response.auth

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a TMDB token on retrofit
 */
data class TokenResponse(
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("expires_at")
    val expires_at: String?,
    @SerializedName("request_token")
    val request_token: String?
)