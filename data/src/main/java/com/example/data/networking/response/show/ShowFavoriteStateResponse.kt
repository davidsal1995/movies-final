package com.example.data.networking.response.show

/**
 * Data class for storing the retrofit favorite state of a show
 */
data class ShowFavoriteStateResponse(
    val favorite: Boolean?
)