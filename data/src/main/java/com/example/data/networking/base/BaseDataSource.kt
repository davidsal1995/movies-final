package com.example.data.networking.base

import com.example.domain.model.DataResource
import retrofit2.Response

/**
 * Provides generic functions to fetch
 * data and convert them to the provided
 * DataResource with T class
 */
abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): DataResource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return DataResource.success(body)
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): DataResource<T> {
        return DataResource.error("Network call has failed for a following reason: $message")
    }

}