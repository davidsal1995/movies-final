package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding an episode details on retrofit
 */
data class EpisodeDetailsResponse(
    @SerializedName("episode_number")
    val episodeNumber: Int?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("still_path")
    val stillPath: String?
)