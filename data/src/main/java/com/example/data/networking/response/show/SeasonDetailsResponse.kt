package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a season details on retrofit
 */
data class SeasonDetailsResponse(
    @SerializedName("name")
    val name: String?,
    @SerializedName("episodes")
    val episodes: List<EpisodeDetailsResponse>?,
    @SerializedName("poster_path")
    val posterPath: String?
)