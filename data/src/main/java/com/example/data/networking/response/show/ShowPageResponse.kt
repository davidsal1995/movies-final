package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a pagination details on retrofit
 */
data class ShowPageResponse(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("results")
    val results: List<ShowEntryResponse>?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("total_results")
    val totalResults: Int?
)