package com.example.data.networking.response.auth

import com.google.gson.annotations.SerializedName

/**
 * Data class for storing the retrofit log out request response
 */
data class LogOutRequestSuccessResponse(
    @SerializedName("success")
    val success: Boolean?
)