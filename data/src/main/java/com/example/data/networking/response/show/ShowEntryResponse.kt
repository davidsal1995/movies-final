package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a show entry from a pagination on retrofit
 */
data class ShowEntryResponse(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?
)