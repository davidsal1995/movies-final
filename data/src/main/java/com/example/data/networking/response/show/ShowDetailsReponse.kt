package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a show details on retrofit
 */
data class ShowDetailsResponse(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("original_name")
    val originalName: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("seasons")
    val seasons: List<ShowSeasonResponse>?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backDropPath: String?
)
