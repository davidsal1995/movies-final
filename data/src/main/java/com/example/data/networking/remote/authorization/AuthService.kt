package com.example.data.networking.remote.authorization

import com.example.data.networking.response.account.AccountResponse
import com.example.data.networking.response.auth.LogOutRequestSuccessResponse
import com.example.data.networking.response.auth.SessionResponse
import com.example.data.networking.response.auth.TokenResponse
import com.example.data.util.constant.API_KEY
import com.example.data.util.constant.AUTH_PATH
import com.example.data.util.constant.DELETE_SESSION_URL
import com.example.data.util.constant.URL_ACCOUNT
import com.example.domain.model.auth.LogOutRequestBody
import retrofit2.Response
import retrofit2.http.*

/**
 * Retrofit interface to handle authentication related operations
 */
interface AuthService {
    /**
     * Get a TMDB token using the API KEY
     */
    @GET("${AUTH_PATH}/token/new?api_key=$API_KEY")
    suspend fun getToken(): Response<TokenResponse>

    /**
     * Get a TMDB session using the API KEY and the provided token
     * @param token is a TMDB token
     */
    @GET("${AUTH_PATH}/session/new?api_key=$API_KEY")
    suspend fun getSession(@Query("request_token") token: String): Response<SessionResponse>

    //    @DELETE(DELETE_SESSION_URL)
    @HTTP(method = "DELETE", path = DELETE_SESSION_URL, hasBody = true)
    suspend fun deleteSession(@Body body: LogOutRequestBody): Response<LogOutRequestSuccessResponse>

    /**
     * Get a TMDB account details using the API KEY and the provided session
     * @param session is a TMDB session
     */
    @GET(URL_ACCOUNT)
    suspend fun getAccount(@Query("session_id") session: String): Response<AccountResponse>
}