package com.example.data.networking.response.account

import com.example.data.networking.response.show.ShowDetailsResponse

/**
 * Data class for storing the model of the account's favorite list
 */
data class AccountFavoritesResponse(
    val results: List<ShowDetailsResponse>?
)
