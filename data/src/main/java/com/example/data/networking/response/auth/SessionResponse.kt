package com.example.data.networking.response.auth

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a TMDB session details on retrofit
 */
data class SessionResponse(
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("session_id")
    val session_id: String?
)