package com.example.data.networking.remote.authorization

import com.example.data.networking.base.BaseDataSource
import com.example.domain.model.auth.LogOutRequestBody

/**
 * Executes the AuthService operations and wraps
 * the result into a DataResource
 */
class AuthRemoteDataSource(
    private val authService: AuthService
) : BaseDataSource() {
    suspend fun getToken() = getResult { authService.getToken() }
    suspend fun getSession(token: String) = getResult { authService.getSession(token) }
    suspend fun deleteSession(body: LogOutRequestBody) =
        getResult { authService.deleteSession(body) }

    suspend fun getAccount(session: String) = getResult { authService.getAccount(session) }
}