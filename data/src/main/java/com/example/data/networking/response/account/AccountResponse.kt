package com.example.data.networking.response.account

import com.google.gson.annotations.SerializedName

/**
 * Retrofit TMDB profile image
 */
data class AvatarTMDBResponse(
    @SerializedName("avatar_path")
    val avatarPath: String?
)

/**
 * Retrofit TMDB avatar
 */
data class AvatarResponse(
    @SerializedName("tmdb")
    val tmdb: AvatarTMDBResponse?
)

/**
 * Retrofit TMDB account details
 */
data class AccountResponse(
    @SerializedName("name")
    val name: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("avatar")
    val avatar: AvatarResponse?,
    @SerializedName("username")
    val username: String?,
)
