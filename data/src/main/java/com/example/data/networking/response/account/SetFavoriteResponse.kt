package com.example.data.networking.response.account

import com.google.gson.annotations.SerializedName

/**
 * Data class for storing the success of setting a show favorite
 */
data class SetFavoriteResponse(
    @SerializedName("success")
    val success: Boolean?
)