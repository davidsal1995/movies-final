package com.example.data.networking.remote.show

import com.example.data.networking.response.account.AccountFavoritesResponse
import com.example.data.networking.response.account.SetFavoriteResponse
import com.example.data.networking.response.show.*
import com.example.data.util.constant.*
import com.example.domain.model.account.FavoriteBody
import retrofit2.Response
import retrofit2.http.*

/**
 * Retrofit interface for handling show related network operations
 */
interface ShowService {
    /**
     * Gets top rated shows
     * @param page is the pagination number
     */
    @GET(URL_TOP_RATED)
    suspend fun getTopRated(@Query("page") page: Int): Response<ShowPageResponse>

    /**
     * Gets popular shows
     * @param page is the pagination number
     */
    @GET(URL_POPULAR)
    suspend fun getPopular(@Query("page") page: Int): Response<ShowPageResponse>

    /**
     * Gets on tv shows
     * @param page is the pagination number
     */
    @GET(URL_ON_TV)
    suspend fun getOnTv(@Query("page") page: Int): Response<ShowPageResponse>

    /**
     * Gets airing today shows
     * @param page is the pagination number
     */
    @GET(URL_AIRING_TODAY)
    suspend fun getAiringToday(@Query("page") page: Int): Response<ShowPageResponse>

    /**
     * Gets a show details
     * @param id is the show id
     */
    @GET(URL_SHOW_DETAILS)
    suspend fun getShowDetails(
        @Path(
            value = "show_id",
            encoded = true
        ) id: Int
    ): Response<ShowDetailsResponse>

    /**
     * Gets a season details
     * @param id is the show id
     * @param number is the season number
     */
    @GET(URL_SEASON_DETAILS)
    suspend fun getSeasonDetails(
        @Path(
            value = "show_id",
            encoded = true
        ) id: Int,
        @Path(
            value = "season_number",
            encoded = true
        ) number: Int
    ): Response<SeasonDetailsResponse>

    /**
     * Gets a show favorite state
     * @param id is the show id
     * @param session is the TMDB session
     */
    @GET(URL_GET_FAVORITE_STATE)
    suspend fun getShowFavoriteState(
        @Path(
            value = "show_id",
            encoded = true
        ) id: Int,
        @Query("session_id") session: String
    ): Response<ShowFavoriteStateResponse>

    /**
     * Sets a show favorite state
     * @param account is the TMDB account
     * @param body is the JSON POST data
     * @param session is the TMDB session
     */
    @POST(URL_SET_FAVORITE_STATE)
    suspend fun setShowFavoriteState(
        @Body body: FavoriteBody,
        @Path(
            value = "account_id",
            encoded = true
        ) account: Int,
        @Query("session_id") session: String
    ): Response<SetFavoriteResponse>

    @GET(URL_GET_FAVORITE_LIST)
    suspend fun getAccountFavorites(
        @Path(
            value = "account_id",
            encoded = true
        ) account: Int,
        @Query("session_id") session: String
    ): Response<AccountFavoritesResponse>
}