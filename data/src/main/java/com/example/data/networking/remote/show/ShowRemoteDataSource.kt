package com.example.data.networking.remote.show

import com.example.data.networking.base.BaseDataSource
import com.example.domain.model.account.FavoriteBody

/**
 * Executes the operations from the ShowService and
 * wraps the result into a DataResource
 */
class ShowRemoteDataSource(
    private val showService: ShowService
) : BaseDataSource() {
    suspend fun getTopRated(page: Int) = getResult { showService.getTopRated(page) }
    suspend fun getPopular(page: Int) = getResult { showService.getPopular(page) }
    suspend fun getOnTv(page: Int) = getResult { showService.getOnTv(page) }
    suspend fun getAiringToday(page: Int) = getResult { showService.getAiringToday(page) }
    suspend fun getShowDetails(id: Int) = getResult { showService.getShowDetails(id) }
    suspend fun getSeasonDetails(id: Int, number: Int) =
        getResult { showService.getSeasonDetails(id, number) }

    suspend fun getShowFavoriteState(id: Int, session: String) =
        getResult { showService.getShowFavoriteState(id, session) }

    suspend fun setShowFavoriteState(account: Int, session: String, body: FavoriteBody) =
        getResult { showService.setShowFavoriteState(body, account, session) }

    suspend fun getAccountFavorites(account: Int, session: String) =
        getResult { showService.getAccountFavorites(account, session) }
}