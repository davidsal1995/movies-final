package com.example.data.networking.response.show

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding a season details on retrofit
 */
data class ShowSeasonResponse(
    @SerializedName("episode_count")
    val episodeCount: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("season_number")
    val seasonNumber: Int?
)
