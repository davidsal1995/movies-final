package com.example.data.util.extension.show

import com.example.data.networking.response.account.SetFavoriteResponse
import com.example.domain.model.DataResource
import com.example.domain.model.account.SetFavorite

fun SetFavoriteResponse.toSetFavorite() = SetFavorite(this.success ?: false)

fun DataResource<SetFavoriteResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toSetFavorite(),
    this.message
)