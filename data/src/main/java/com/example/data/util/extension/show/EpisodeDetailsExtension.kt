package com.example.data.util.extension.show

import com.example.data.networking.response.show.EpisodeDetailsResponse
import com.example.domain.model.show.EpisodeDetails

/**
 * Converts a Retrofit EpisodeDetails into a Model EpisodeDetails
 */
fun EpisodeDetailsResponse.toEpisodeDetails() = EpisodeDetails(
    this.episodeNumber ?: 0,
    this.overview ?: "",
    this.stillPath ?: ""
)

/**
 * Converts a list of Retrofit EpisodeDetail into a list of Model EpisodeDetails
 */
fun List<EpisodeDetailsResponse>.toEpisodeDetails() = this.map { it.toEpisodeDetails() }