package com.example.data.util.extension.show

import com.example.data.networking.response.show.ShowPageResponse
import com.example.domain.model.DataResource
import com.example.domain.model.show.ShowPage

/**
 * Converts a Retrofit ShowPage into a Model ShowPage
 */
fun ShowPageResponse.toShowPage() = ShowPage(
    this.page ?: 1,
    this.results?.toShowEntry() ?: listOf(),
    this.totalPages ?: 0,
    this.totalResults ?: 0
)

/**
 * Converts a Retrofit ShowPage DataResource into a Model ShowPage DataResource
 */
fun DataResource<ShowPageResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toShowPage(),
    this.message
)