package com.example.data.util.extension.show

import com.example.data.database.entity.ShowEntity
import com.example.data.networking.response.show.ShowEntryResponse
import com.example.domain.model.show.ShowEntry

/**
 * Converts a Retrofit ShowEntry into a Model ShowEntry
 */
fun ShowEntryResponse.toShowEntry() = ShowEntry(
    this.id ?: 0,
    this.posterPath ?: "",
    this.name ?: "",
    this.voteAverage ?: 0.0
)

/**
 * Converts a list of Retrofit ShowEntry into a list of Model ShowEntry
 */
fun List<ShowEntryResponse>.toShowEntry() = this.map { it.toShowEntry() }

/**
 * Convert a ShowEntry into a Show Entity
 * @param page is the pagination
 * @param tag is the show tag
 */
fun ShowEntry.toShowEntity(page: Int, tag: String) = ShowEntity(
    this.id,
    this.posterPath,
    this.name,
    this.voteAverage,
    page,
    tag
)

/**
 * Convert a list of Show Entry to a list of Show Entity
 * @param page is the pagination
 * @param tag is the show tag
 */
fun List<ShowEntry>.toShowEntity(page: Int, tag: String) = this.map { it.toShowEntity(page, tag) }