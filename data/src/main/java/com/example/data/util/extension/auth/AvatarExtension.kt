package com.example.data.util.extension.auth

import com.example.data.networking.response.account.AccountResponse
import com.example.data.networking.response.account.AvatarResponse
import com.example.data.networking.response.account.AvatarTMDBResponse
import com.example.domain.model.DataResource
import com.example.domain.model.account.Account
import com.example.domain.model.account.Avatar
import com.example.domain.model.account.AvatarTMDB

/**
 * Converts a Retrofit AvatarTMDB into its model
 */
fun AvatarTMDBResponse.toAvatarTMDB() = AvatarTMDB(this.avatarPath ?: "")

/**
 * Converts a Retrofit Avatar Response into its model
 */
fun AvatarResponse.toAvatar() = Avatar(this.tmdb?.toAvatarTMDB() ?: AvatarTMDB(""))

/**
 * Converts a Retrofit Account Response  into its model
 */
fun AccountResponse.toAccount() = Account(
    this.name ?: "",
    this.id ?: -1,
    this.avatar?.toAvatar() ?: Avatar(AvatarTMDB("")),
    this.username ?: ""
)

/**
 * Converts a Retrofit Account Response Data Resource into its model
 */
fun DataResource<AccountResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toAccount(),
    this.message
)