package com.example.data.util.test

import com.example.domain.model.DataResource
import com.example.domain.model.account.Account
import com.example.domain.model.account.Avatar
import com.example.domain.model.account.AvatarTMDB
import com.example.domain.model.auth.LogOutRequestBody
import com.example.domain.model.auth.LogOutRequestSuccess
import com.example.domain.model.auth.Session
import com.example.domain.model.auth.Token
import com.example.domain.repository.AuthRepository

class FakeAuthRepositoryImpl(val status: DataResource.Status, val message: String) :
    AuthRepository {
    override suspend fun getToken(): DataResource<Token> =
        DataResource(status, Token(true, "expires at", "request token"), message)

    override suspend fun getSession(token: String): DataResource<Session> =
        DataResource(status, Session(true, "session id"), message)

    override suspend fun getAccount(session: String): DataResource<Account> = DataResource(
        status,
        Account(
            "name",
            1,
            Avatar(AvatarTMDB("avatar path")),
            "username"
        ),
        message
    )

    override suspend fun deleteSession(body: LogOutRequestBody): DataResource<LogOutRequestSuccess> =
        DataResource(status, LogOutRequestSuccess(true), message)
}