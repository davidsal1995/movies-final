package com.example.data.util.extension.account

import com.example.data.networking.response.account.AccountFavoritesResponse
import com.example.data.util.extension.show.toShowDetails
import com.example.domain.model.DataResource
import com.example.domain.model.account.AccountFavorites

fun AccountFavoritesResponse.toAccountFavorites() = AccountFavorites(
    this.results?.toShowDetails() ?: listOf()
)

fun DataResource<AccountFavoritesResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toAccountFavorites(),
    this.message
)