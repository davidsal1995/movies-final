package com.example.data.util.extension.show

import com.example.data.networking.response.show.ShowFavoriteStateResponse
import com.example.domain.model.DataResource
import com.example.domain.model.show.ShowFavoriteState

fun ShowFavoriteStateResponse.toFavoriteStateResponse() = ShowFavoriteState(this.favorite ?: false)

fun DataResource<ShowFavoriteStateResponse>.toDataResource() =
    DataResource(
        this.status,
        this.data?.toFavoriteStateResponse(),
        this.message
    )