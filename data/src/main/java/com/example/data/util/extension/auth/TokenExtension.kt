package com.example.data.util.extension.auth

import com.example.data.networking.response.auth.TokenResponse
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Token

/**
 * Converts a Retrofit Token into a Model Token
 */
fun TokenResponse.toToken() = Token(
    this.success ?: false,
    this.expires_at ?: "",
    this.request_token ?: ""
)

/**
 * Converts a Retrofit Token DataResource into a Model Token DataResource
 */
fun DataResource<TokenResponse>.toDataResource() =
    DataResource(this.status, this.data?.toToken(), this.message)