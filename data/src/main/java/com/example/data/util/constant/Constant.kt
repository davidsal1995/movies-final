package com.example.data.util.constant

// TMDB API URL
const val API_URL = "https://api.themoviedb.org/3/"

// URL parameters
const val QUERY_LANGUAGE = "language=en-US"
const val API_KEY = "5a9c62ff5ff1273c0d211ca22832dbb6"

// Auth related URL
const val AUTH_URL = "https://www.themoviedb.org/authenticate/"
const val AUTH_PATH = "authentication"
const val URL_ACCOUNT = "${API_URL}account?api_key=${API_KEY}"
const val DELETE_SESSION_URL =
    "${API_URL}${AUTH_PATH}/session?api_key=${API_KEY}"


// Favorites related URL
const val URL_GET_FAVORITE_LIST =
    "${API_URL}account/{account_id}/favorite/tv?api_key=${API_KEY}&language=en-US&sort_by=created_at.desc"
const val URL_GET_FAVORITE_STATE =
    "${API_URL}tv/{show_id}/account_states?api_key=${API_KEY}&${QUERY_LANGUAGE}"
const val URL_SET_FAVORITE_STATE =
    "${API_URL}account/{account_id}/favorite?api_key=${API_KEY}"

// Show categories related URL
const val URL_TOP_RATED = "${API_URL}tv/top_rated?api_key=${API_KEY}&${QUERY_LANGUAGE}"
const val URL_POPULAR = "${API_URL}tv/popular?api_key=${API_KEY}&${QUERY_LANGUAGE}"
const val URL_ON_TV = "${API_URL}tv/on_the_air?api_key=${API_KEY}&${QUERY_LANGUAGE}"
const val URL_AIRING_TODAY = "${API_URL}tv/airing_today?api_key=${API_KEY}&${QUERY_LANGUAGE}"

// URL for show details
const val URL_SHOW_DETAILS =
    "${API_URL}tv/{show_id}?api_key=${API_KEY}&${QUERY_LANGUAGE}"

// URL for season details
const val URL_SEASON_DETAILS =
    "${API_URL}tv/{show_id}/season/{season_number}?api_key=${API_KEY}&${QUERY_LANGUAGE}"