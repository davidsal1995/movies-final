package com.example.data.util.extension.auth

import com.example.data.networking.response.auth.LogOutRequestSuccessResponse
import com.example.domain.model.DataResource
import com.example.domain.model.auth.LogOutRequestSuccess

fun LogOutRequestSuccessResponse.toLogOutRequestSuccess() = LogOutRequestSuccess(
    this.success ?: false
)

fun DataResource<LogOutRequestSuccessResponse>.toLogOutRequestSuccess() = DataResource(
    this.status,
    this.data?.toLogOutRequestSuccess(),
    this.message
)