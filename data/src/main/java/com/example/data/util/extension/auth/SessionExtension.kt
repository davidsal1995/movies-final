package com.example.data.util.extension.auth

import com.example.data.networking.response.auth.SessionResponse
import com.example.domain.model.DataResource
import com.example.domain.model.auth.Session

/**
 * Converts a Retrofit Session into a Model Session
 */
fun SessionResponse.toSession() = Session(
    this.success ?: false,
    this.session_id ?: ""
)

/**
 * Converts a Retrofit Session DataResource into a Model Session DataResource
 */
fun DataResource<SessionResponse>.toDataResource() =
    DataResource(this.status, this.data?.toSession(), this.message)