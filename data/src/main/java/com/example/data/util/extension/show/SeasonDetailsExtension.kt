package com.example.data.util.extension.show

import com.example.data.networking.response.show.SeasonDetailsResponse
import com.example.domain.model.DataResource
import com.example.domain.model.show.SeasonDetails

/**
 * Converts a Retrofit SeasonDetails into a Model SeasonDetails
 */
fun SeasonDetailsResponse.toSeasonDetail() = SeasonDetails(
    this.name ?: "",
    this.episodes?.toEpisodeDetails() ?: listOf(),
    this.posterPath ?: ""
)

/**
 * Converts a Retrofit SeasonDetails DataResource into a Model SeasonDetails DataResource
 */
fun DataResource<SeasonDetailsResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toSeasonDetail(),
    this.message
)