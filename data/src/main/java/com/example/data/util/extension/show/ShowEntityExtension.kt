package com.example.data.util.extension.show

import com.example.data.database.entity.ShowEntity
import com.example.domain.model.show.ShowEntry

/**
 * Converts a room show entity into a show entry
 */
fun ShowEntity.toShowEntry() = ShowEntry(
    this.id,
    this.posterPath,
    this.name,
    this.voteAverage
)

/**
 * Converts a list of room show entity into a show entry
 */
fun List<ShowEntity>.toShowEntry() = this.map { it.toShowEntry() }