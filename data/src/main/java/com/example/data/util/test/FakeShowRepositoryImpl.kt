package com.example.data.util.test

import com.example.data.database.entity.ShowEntity
import com.example.data.util.extension.show.toShowEntity
import com.example.data.util.extension.show.toShowEntry
import com.example.domain.model.DataResource
import com.example.domain.model.account.AccountFavorites
import com.example.domain.model.account.FavoriteBody
import com.example.domain.model.account.SetFavorite
import com.example.domain.model.show.*
import com.example.domain.repository.ShowRepository

class FakeShowRepositoryImpl(val status: DataResource.Status, val message: String) :
    ShowRepository {
    override suspend fun getTopRated(page: Int): DataResource<ShowPage> = DataResource(
        status, ShowPage(
            1,
            listOf(ShowEntry(1, "poster path", "name", 1.1)), 1, 1
        ), message
    )

    override suspend fun getPopular(page: Int): DataResource<ShowPage> = DataResource(
        status, ShowPage(
            1,
            listOf(ShowEntry(1, "poster path", "name", 1.1)), 1, 1
        ), message
    )

    override suspend fun getOnTv(page: Int): DataResource<ShowPage> = DataResource(
        status, ShowPage(
            1,
            listOf(ShowEntry(1, "poster path", "name", 1.1)), 1, 1
        ), message
    )


    override suspend fun getAiringToday(page: Int): DataResource<ShowPage> = DataResource(
        status, ShowPage(
            1,
            listOf(ShowEntry(1, "poster path", "name", 1.1)), 1, 1
        ), message
    )

    override suspend fun getShowDetails(id: Int): DataResource<ShowDetails> =
        DataResource(
            status, ShowDetails(
                1,
                "name",
                "original name",
                1.1,
                "overview",
                listOf(ShowSeason(1, "name", "overview", "poster path", 1)),
                "poster path",
                "back drop path"
            ), message
        )

    override suspend fun getSeasonDetails(id: Int, number: Int): DataResource<SeasonDetails> =
        DataResource(
            status,
            SeasonDetails(
                "name", listOf(
                    EpisodeDetails(1, "overview", "still path")
                ), "poster path"
            ), message
        )

    override suspend fun getShowFavoriteState(
        id: Int,
        session: String
    ): DataResource<ShowFavoriteState> = DataResource(status, ShowFavoriteState(true), message)

    override suspend fun setShowFavoriteState(
        account: Int,
        session: String,
        body: FavoriteBody
    ): DataResource<SetFavorite> = DataResource(status, SetFavorite(true), message)

    override suspend fun getAccountFavorites(
        id: Int,
        session: String
    ): DataResource<AccountFavorites> = DataResource(
        status, AccountFavorites(
            listOf(
                ShowDetails(
                    1,
                    "name",
                    "original name",
                    1.1,
                    "overview",
                    listOf(ShowSeason(1, "name", "overview", "poster path", 1)),
                    "poster path",
                    "back drop path"
                )
            )
        ), message
    )

    // Simulate Room Storage

    var storedShows = listOf<ShowEntity>()

    override suspend fun insertShowList(shows: List<ShowEntry>, page: Int, tag: String) {
        storedShows = shows.toShowEntity(page, tag)
    }

    override suspend fun getShowListTagAll(tag: String): List<ShowEntry> =
        storedShows.filter { it.tag == tag }.toShowEntry()
}