package com.example.data.util.extension.show

import com.example.data.networking.response.show.ShowSeasonResponse
import com.example.domain.model.show.ShowSeason

/**
 * Converts a Retrofit ShowSeason into a Model ShowSeason
 */
fun ShowSeasonResponse.toShowSeason() = ShowSeason(
    this.episodeCount ?: 0,
    this.name ?: "",
    this.overview ?: "",
    this.posterPath ?: "",
    this.seasonNumber ?: 0
)

/**
 * Converts a list of Retrofit ShowSeason into a list of Model ShowSeason
 */
fun List<ShowSeasonResponse>.toShowSeason() = this.map { it.toShowSeason() }