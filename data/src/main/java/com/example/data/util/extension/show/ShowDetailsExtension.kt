package com.example.data.util.extension.show

import com.example.data.networking.response.show.ShowDetailsResponse
import com.example.domain.model.DataResource
import com.example.domain.model.show.ShowDetails

/**
 * Converts a Retrofit ShowDetails into a Model ShowDetails
 */
fun ShowDetailsResponse.toShowDetails() = ShowDetails(
    this.id ?: -1,
    this.name ?: "",
    this.originalName ?: "",
    this.voteAverage ?: 0.0,
    this.overview ?: "",
    this.seasons?.toShowSeason() ?: listOf(),
    this.posterPath ?: "",
    this.backDropPath ?: ""
)

/**
 * Converts a Retrofit ShowDetails DataResource into a Model ShowDetails DataResource
 */
fun DataResource<ShowDetailsResponse>.toDataResource() = DataResource(
    this.status,
    this.data?.toShowDetails(),
    this.message
)

/**
 * Converts a list of show details response into the model equivalent
 */
fun List<ShowDetailsResponse>.toShowDetails() = this.map {
    ShowDetails(
        it.id ?: -1,
        it.name ?: "",
        it.originalName ?: "",
        it.voteAverage ?: 0.0,
        it.overview ?: "",
        it.seasons?.toShowSeason() ?: listOf(),
        it.posterPath ?: "",
        it.backDropPath ?: ""
    )
}