package com.example.data.repository

import com.example.data.networking.remote.authorization.AuthRemoteDataSource
import com.example.data.util.extension.auth.toDataResource
import com.example.data.util.extension.auth.toLogOutRequestSuccess
import com.example.domain.model.DataResource
import com.example.domain.model.account.Account
import com.example.domain.model.auth.LogOutRequestBody
import com.example.domain.model.auth.LogOutRequestSuccess
import com.example.domain.model.auth.Session
import com.example.domain.model.auth.Token
import com.example.domain.repository.AuthRepository

/**
 * AuthRepository implementation
 * Converts retrofit results into data model
 */
class AuthRepositoryImpl(
    private val remoteDataSource: AuthRemoteDataSource
) : AuthRepository {
    override suspend fun getToken(): DataResource<Token> =
        remoteDataSource.getToken().toDataResource()

    override suspend fun getSession(token: String): DataResource<Session> =
        remoteDataSource.getSession(token).toDataResource()

    override suspend fun getAccount(session: String): DataResource<Account> =
        remoteDataSource.getAccount(session).toDataResource()

    override suspend fun deleteSession(body: LogOutRequestBody): DataResource<LogOutRequestSuccess> =
        remoteDataSource.deleteSession(body).toLogOutRequestSuccess()
}