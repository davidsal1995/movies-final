package com.example.data.repository

import com.example.data.database.dao.ShowDao
import com.example.data.networking.remote.show.ShowRemoteDataSource
import com.example.data.util.extension.account.toDataResource
import com.example.data.util.extension.show.toDataResource
import com.example.data.util.extension.show.toShowEntity
import com.example.data.util.extension.show.toShowEntry
import com.example.domain.model.DataResource
import com.example.domain.model.account.AccountFavorites
import com.example.domain.model.account.FavoriteBody
import com.example.domain.model.account.SetFavorite
import com.example.domain.model.show.*
import com.example.domain.repository.ShowRepository

/**
 * ShowRepository implementation
 * Converts retrofit results into data model
 */
class ShowRepositoryImpl(
    private val remoteDataSource: ShowRemoteDataSource,
    private val localDataSource: ShowDao
) : ShowRepository {
    override suspend fun getTopRated(page: Int): DataResource<ShowPage> =
        remoteDataSource.getTopRated(page).toDataResource()

    override suspend fun getPopular(page: Int): DataResource<ShowPage> =
        remoteDataSource.getPopular(page).toDataResource()

    override suspend fun getOnTv(page: Int): DataResource<ShowPage> =
        remoteDataSource.getOnTv(page).toDataResource()

    override suspend fun getAiringToday(page: Int): DataResource<ShowPage> =
        remoteDataSource.getAiringToday(page).toDataResource()

    override suspend fun getShowDetails(id: Int): DataResource<ShowDetails> =
        remoteDataSource.getShowDetails(id).toDataResource()

    override suspend fun getSeasonDetails(id: Int, number: Int): DataResource<SeasonDetails> =
        remoteDataSource.getSeasonDetails(id, number).toDataResource()

    override suspend fun getShowFavoriteState(
        id: Int,
        session: String
    ): DataResource<ShowFavoriteState> =
        remoteDataSource.getShowFavoriteState(id, session).toDataResource()

    override suspend fun setShowFavoriteState(
        account: Int,
        session: String,
        body: FavoriteBody
    ): DataResource<SetFavorite> =
        remoteDataSource.setShowFavoriteState(account, session, body).toDataResource()

    override suspend fun getAccountFavorites(
        id: Int,
        session: String
    ): DataResource<AccountFavorites> =
        remoteDataSource.getAccountFavorites(id, session).toDataResource()

    override suspend fun insertShowList(shows: List<ShowEntry>, page: Int, tag: String) =
        localDataSource.insertAll(shows.toShowEntity(page, tag))

    override suspend fun getShowListTagAll(tag: String): List<ShowEntry> =
        localDataSource.getShowListTagAll(tag).toShowEntry()
}

