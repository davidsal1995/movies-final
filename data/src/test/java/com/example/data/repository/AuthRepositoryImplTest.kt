package com.example.data.repository

import com.example.data.util.test.FakeAuthRepositoryImpl
import com.example.domain.model.DataResource
import com.example.domain.model.auth.LogOutRequestBody
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking

class AuthRepositoryImplTest : TestCase() {

    private val statusList = DataResource.Status.values()

    private val fakeAuthRepositoryList = statusList.map { FakeAuthRepositoryImpl(it, it.value) }

    fun testGetToken() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeAuthRepositoryList[it.index].getToken()
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.success, true)
                assertEquals(testData.data?.request_token, "request token")
                assertEquals(testData.data?.expires_at, "expires at")
            }
        }
    }

    fun testGetSession() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeAuthRepositoryList[it.index].getSession("token")
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.success, true)
                assertEquals(testData.data?.session_id, "session id")
            }
        }
    }

    fun testGetAccount() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeAuthRepositoryList[it.index].getAccount("session id")
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.name, "name")
                assertEquals(testData.data?.id, 1)
                assertEquals(testData.data?.avatar?.tmdb?.avatarPath, "avatar path")
                assertEquals(testData.data?.username, "username")
            }
        }
    }

    fun testDeleteSession() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData =
                    fakeAuthRepositoryList[it.index].deleteSession(LogOutRequestBody("session id"))
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.success, true)
            }
        }
    }
}