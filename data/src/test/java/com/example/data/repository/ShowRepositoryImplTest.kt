package com.example.data.repository

import com.example.data.util.test.FakeShowRepositoryImpl
import com.example.domain.model.DataResource
import com.example.domain.model.account.FavoriteBody
import com.example.domain.model.show.ShowEntry
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking

class ShowRepositoryImplTest : TestCase() {

    private val showList = listOf(
        ShowEntry(1, "poster path", "name", 1.1),
        ShowEntry(2, "poster path", "name", 2.2),
    )

    private val statusList = DataResource.Status.values()

    private val fakeShowRepositoryList = statusList.map { FakeShowRepositoryImpl(it, it.value) }

    fun testGetTopRated() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getTopRated(1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.page, 1)
                assertEquals(testData.data?.totalPages, 1)
                assertEquals(testData.data?.totalResults, 1)

                assertEquals(testData.data?.results?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.id, 1)
                assertEquals(testData.data?.results?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.results?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.voteAverage, 1.1)
            }
        }
    }

    fun testGetPopular() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getPopular(1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.page, 1)
                assertEquals(testData.data?.totalPages, 1)
                assertEquals(testData.data?.totalResults, 1)

                assertEquals(testData.data?.results?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.id, 1)
                assertEquals(testData.data?.results?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.results?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.voteAverage, 1.1)
            }
        }
    }

    fun testGetOnTv() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getOnTv(1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.page, 1)
                assertEquals(testData.data?.totalPages, 1)
                assertEquals(testData.data?.totalResults, 1)

                assertEquals(testData.data?.results?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.id, 1)
                assertEquals(testData.data?.results?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.results?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.voteAverage, 1.1)
            }
        }
    }

    fun testGetAiringToday() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getAiringToday(1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.page, 1)
                assertEquals(testData.data?.totalPages, 1)
                assertEquals(testData.data?.totalResults, 1)

                assertEquals(testData.data?.results?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.id, 1)
                assertEquals(testData.data?.results?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.results?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.voteAverage, 1.1)
            }
        }
    }

    fun testGetShowDetails() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getShowDetails(1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.id, 1)
                assertEquals(testData.data?.name, "name")
                assertEquals(testData.data?.originalName, "original name")
                assertEquals(testData.data?.voteAverage, 1.1)
                assertEquals(testData.data?.overview, "overview")
                assertEquals(testData.data?.posterPath, "poster path")
                assertEquals(testData.data?.backDropPath, "back drop path")

                assertEquals(testData.data?.seasons?.size, 1)
                assertEquals(testData.data?.seasons?.get(0)?.episodeCount, 1)
                assertEquals(testData.data?.seasons?.get(0)?.name, "name")
                assertEquals(testData.data?.seasons?.get(0)?.overview, "overview")
                assertEquals(testData.data?.seasons?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.seasons?.get(0)?.seasonNumber, 1)
            }
        }
    }

    fun testGetSeasonDetails() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getSeasonDetails(1, 1)
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.name, "name")
                assertEquals(testData.data?.posterPath, "poster path")

                assertEquals(testData.data?.episodes?.size, 1)
                assertEquals(testData.data?.episodes?.get(0)?.episodeNumber, 1)
                assertEquals(testData.data?.episodes?.get(0)?.overview, "overview")
                assertEquals(testData.data?.episodes?.get(0)?.stillPath, "still path")
            }
        }
    }

    fun testGetShowFavoriteState() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData =
                    fakeShowRepositoryList[it.index].getShowFavoriteState(1, "session id")
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.favorite, true)
            }
        }
    }

    fun testSetShowFavoriteState() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].setShowFavoriteState(
                    1,
                    "session id",
                    FavoriteBody(1, true, "tv")
                )
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.success, true)
            }
        }
    }

    fun testGetAccountFavorites() {
        runBlocking {
            statusList.withIndex().forEach {
                val testData = fakeShowRepositoryList[it.index].getAccountFavorites(1, "session id")
                val currentStatus = statusList[it.index]

                assertEquals(testData.status, currentStatus)
                assertEquals(testData.message, currentStatus.value)

                assertEquals(testData.data?.results?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.id, 1)
                assertEquals(testData.data?.results?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.originalName, "original name")
                assertEquals(testData.data?.results?.get(0)?.voteAverage, 1.1)
                assertEquals(testData.data?.results?.get(0)?.overview, "overview")
                assertEquals(testData.data?.results?.get(0)?.posterPath, "poster path")
                assertEquals(testData.data?.results?.get(0)?.backDropPath, "back drop path")

                assertEquals(testData.data?.results?.get(0)?.seasons?.size, 1)
                assertEquals(testData.data?.results?.get(0)?.seasons?.get(0)?.episodeCount, 1)
                assertEquals(testData.data?.results?.get(0)?.seasons?.get(0)?.name, "name")
                assertEquals(testData.data?.results?.get(0)?.seasons?.get(0)?.overview, "overview")
                assertEquals(
                    testData.data?.results?.get(0)?.seasons?.get(0)?.posterPath,
                    "poster path"
                )
                assertEquals(testData.data?.results?.get(0)?.seasons?.get(0)?.seasonNumber, 1)
            }
        }
    }

    fun testInsertShowList() {

        runBlocking {
            statusList.withIndex().forEach {

                fakeShowRepositoryList[it.index].insertShowList(
                    showList, 1, "tag"
                )

                assertEquals(fakeShowRepositoryList[it.index].storedShows.size, showList.size)
            }
        }
    }

    fun testGetShowListTagAll() {
        runBlocking {
            statusList.withIndex().forEach {

                fakeShowRepositoryList[it.index].insertShowList(
                    listOf(showList[0]), 1, "tag"
                )

                val testData = fakeShowRepositoryList[it.index].getShowListTagAll("tag")
                assertEquals(testData.size, 1)
            }
        }
    }
}