package com.example.domain.model.auth

import junit.framework.TestCase

class LogOutRequestSuccessTest : TestCase() {

    private val testLogOutRequestSuccess = LogOutRequestSuccess(true)

    fun testGetLogOutRequestSuccess() {
        assertEquals(testLogOutRequestSuccess.success, true)
    }
}