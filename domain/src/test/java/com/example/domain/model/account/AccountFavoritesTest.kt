package com.example.domain.model.account

import com.example.domain.model.show.ShowDetails
import com.example.domain.model.show.ShowSeason
import junit.framework.TestCase

class AccountFavoritesTest : TestCase() {

    private val accountFavoritesTest = AccountFavorites(
        listOf(
            ShowDetails(
                1,
                "name",
                "original name",
                1.1,
                "overview",
                listOf(ShowSeason(1, "name", "overview", "poster path", 1)),
                "poster path",
                "back drop path"
            )
        )
    )

    fun testGetAccountFavorites() {
        assertEquals(accountFavoritesTest.results.size, 1)
        assertEquals(accountFavoritesTest.results[0].id, 1)
        assertEquals(accountFavoritesTest.results[0].name, "name")
        assertEquals(accountFavoritesTest.results[0].originalName, "original name")
        assertEquals(accountFavoritesTest.results[0].voteAverage, 1.1)
        assertEquals(accountFavoritesTest.results[0].overview, "overview")

        assertEquals(accountFavoritesTest.results[0].seasons.size, 1)
        assertEquals(accountFavoritesTest.results[0].seasons[0].episodeCount, 1)
        assertEquals(accountFavoritesTest.results[0].seasons[0].name, "name")
        assertEquals(accountFavoritesTest.results[0].seasons[0].overview, "overview")
        assertEquals(accountFavoritesTest.results[0].seasons[0].posterPath, "poster path")
        assertEquals(accountFavoritesTest.results[0].seasons[0].seasonNumber, 1)

        assertEquals(accountFavoritesTest.results[0].posterPath, "poster path")
        assertEquals(accountFavoritesTest.results[0].backDropPath, "back drop path")
    }
}