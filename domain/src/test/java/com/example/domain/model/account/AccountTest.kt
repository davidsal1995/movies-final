package com.example.domain.model.account

import junit.framework.TestCase

class AccountTest : TestCase() {

    private val accountTest = Account("name", 1, Avatar(AvatarTMDB("avatar path")), "username")

    fun testGetAccount() {
        assertEquals(accountTest.name, "name")
        assertEquals(accountTest.id, 1)
        assertEquals(accountTest.avatar.tmdb.avatarPath, "avatar path")
        assertEquals(accountTest.username, "username")
    }
}