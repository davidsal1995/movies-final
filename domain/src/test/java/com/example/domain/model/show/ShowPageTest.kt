package com.example.domain.model.show

import junit.framework.TestCase

class ShowPageTest : TestCase() {

    private val testShowPageTest = ShowPage(
        1,
        listOf(
            ShowEntry(1, "poster path", "name", 1.1)
        ),
        1,
        1
    )

    fun testGetShowPage() {
        assertEquals(testShowPageTest.page, 1)
        assertEquals(testShowPageTest.totalPages, 1)
        assertEquals(testShowPageTest.totalResults, 1)

        assertEquals(testShowPageTest.results.size, 1)
        assertEquals(testShowPageTest.results[0].id, 1)
        assertEquals(testShowPageTest.results[0].posterPath, "poster path")
        assertEquals(testShowPageTest.results[0].name, "name")
        assertEquals(testShowPageTest.results[0].voteAverage, 1.1)
    }
}