package com.example.domain.model.show

import junit.framework.TestCase

class ShowFavoriteStateTest : TestCase() {

    private val testShowFavoriteState = ShowFavoriteState(true)

    fun testGetShowFavoriteState() {
        assertEquals(testShowFavoriteState.favorite, true)
    }
}