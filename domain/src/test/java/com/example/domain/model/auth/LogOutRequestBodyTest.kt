package com.example.domain.model.auth

import junit.framework.TestCase

class LogOutRequestBodyTest : TestCase() {

    private val testLogOutRequestBody = LogOutRequestBody("session id")

    fun testGetLogOutRequestBody() {
        assertEquals(testLogOutRequestBody.session_id, "session id")
    }
}