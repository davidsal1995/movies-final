package com.example.domain.model.account

import junit.framework.TestCase

class FavoriteBodyTest : TestCase() {

    private val testFavoriteBody = FavoriteBody(1, true, "tv")

    fun testGetFavoriteBody() {
        assertEquals(testFavoriteBody.media_id, 1)
        assertEquals(testFavoriteBody.favorite, true)
        assertEquals(testFavoriteBody.media_type, "tv")
    }
}