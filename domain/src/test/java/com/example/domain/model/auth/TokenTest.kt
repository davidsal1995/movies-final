package com.example.domain.model.auth

import junit.framework.TestCase

class TokenTest : TestCase() {

    private val testToken = Token(true, "expires at", "request token")

    fun testGetTestToken() {
        assertEquals(testToken.success, true)
        assertEquals(testToken.expires_at, "expires at")
        assertEquals(testToken.request_token, "request token")
    }
}