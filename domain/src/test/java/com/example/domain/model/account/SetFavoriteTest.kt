package com.example.domain.model.account

import junit.framework.TestCase

class SetFavoriteTest : TestCase() {

    private val testSetFavorite = SetFavorite(true)

    fun testGetSetFavorite() {
        assertEquals(testSetFavorite.success, true)
    }
}