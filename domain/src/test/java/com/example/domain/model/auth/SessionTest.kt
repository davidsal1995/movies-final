package com.example.domain.model.auth

import junit.framework.TestCase

class SessionTest : TestCase() {

    private val testSession = Session(true, "session id")

    fun testGetTestSession() {
        assertEquals(testSession.success, true)
        assertEquals(testSession.session_id, "session id")
    }
}