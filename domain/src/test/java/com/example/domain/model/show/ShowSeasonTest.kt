package com.example.domain.model.show

import junit.framework.TestCase

class ShowSeasonTest : TestCase() {

    private val testShowSeason = ShowSeason(1, "name", "overview", "poster path", 1)

    fun testGetShowSeason() {
        assertEquals(testShowSeason.episodeCount, 1)
        assertEquals(testShowSeason.name, "name")
        assertEquals(testShowSeason.overview, "overview")
        assertEquals(testShowSeason.posterPath, "poster path")
        assertEquals(testShowSeason.seasonNumber, 1)
    }
}