package com.example.domain.model.show

import junit.framework.TestCase

class ShowDetailsTest : TestCase() {

    private val testShowDetails = ShowDetails(
        1, "name", "original name", 1.1, "overview",
        listOf(
            ShowSeason(1, "name", "overview", "poster path", 1)
        ),
        "poster path",
        "back drop path"
    )

    fun testGetShowDetails() {
        assertEquals(testShowDetails.id, 1)
        assertEquals(testShowDetails.name, "name")
        assertEquals(testShowDetails.originalName, "original name")
        assertEquals(testShowDetails.voteAverage, 1.1)
        assertEquals(testShowDetails.overview, "overview")

        assertEquals(testShowDetails.seasons.size, 1)
        assertEquals(testShowDetails.seasons[0].episodeCount, 1)
        assertEquals(testShowDetails.seasons[0].name, "name")
        assertEquals(testShowDetails.seasons[0].overview, "overview")
        assertEquals(testShowDetails.seasons[0].posterPath, "poster path")
        assertEquals(testShowDetails.seasons[0].seasonNumber, 1)


        assertEquals(testShowDetails.posterPath, "poster path")
        assertEquals(testShowDetails.backDropPath, "back drop path")
    }
}