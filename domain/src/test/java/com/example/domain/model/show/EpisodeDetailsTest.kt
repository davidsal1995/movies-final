package com.example.domain.model.show

import junit.framework.TestCase

class EpisodeDetailsTest : TestCase() {

    private val testEpisodeDetails = EpisodeDetails(1, "overview", "still path")

    fun testGetTestEpisodeDetails() {
        assertEquals(testEpisodeDetails.episodeNumber, 1)
        assertEquals(testEpisodeDetails.overview, "overview")
        assertEquals(testEpisodeDetails.stillPath, "still path")
    }
}