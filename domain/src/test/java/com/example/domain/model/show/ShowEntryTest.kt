package com.example.domain.model.show

import junit.framework.TestCase

class ShowEntryTest : TestCase() {

    private val testShowEntry = ShowEntry(1, "poster path", "name", 1.1)

    fun testGetShowEntry() {
        assertEquals(testShowEntry.id, 1)
        assertEquals(testShowEntry.posterPath, "poster path")
        assertEquals(testShowEntry.name, "name")
        assertEquals(testShowEntry.voteAverage, 1.1)
    }
}