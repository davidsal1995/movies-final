package com.example.domain.model.show

import junit.framework.TestCase

class SeasonDetailsTest : TestCase() {

    private val testSeasonDetails = SeasonDetails(
        "name",
        listOf(
            EpisodeDetails(1, "overview", "still path")
        ),
        "poster path"
    )

    fun testGetSeasonDetails() {
        assertEquals(testSeasonDetails.name, "name")

        assertEquals(testSeasonDetails.episodes.size, 1)
        assertEquals(testSeasonDetails.episodes[0].episodeNumber, 1)
        assertEquals(testSeasonDetails.episodes[0].overview, "overview")
        assertEquals(testSeasonDetails.episodes[0].stillPath, "still path")

        assertEquals(testSeasonDetails.posterPath, "poster path")
    }
}