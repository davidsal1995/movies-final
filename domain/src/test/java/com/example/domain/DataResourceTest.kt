package com.example.domain

import com.example.domain.model.DataResource
import junit.framework.TestCase

class DataResourceTest : TestCase() {

    private val dataTestError = DataResource.error("Error Message", "Error")
    private val dataTestLoading = DataResource.loading("Loading")
    private val dataTestLoadingMore = DataResource.loadingMore("Loading More")
    private val dataTestSuccess = DataResource.success("Success")
    private val dataTestDone = DataResource.done("Done")

    fun testGetStatus() {
        assertEquals(DataResource.Status.ERROR, dataTestError.status)
        assertEquals(DataResource.Status.LOADING, dataTestLoading.status)
        assertEquals(DataResource.Status.LOADING_MORE, dataTestLoadingMore.status)
        assertEquals(DataResource.Status.SUCCESS, dataTestSuccess.status)
        assertEquals(DataResource.Status.DONE, dataTestDone.status)
    }

    fun testGetData() {
        assertEquals("Error", dataTestError.data)
        assertEquals("Loading", dataTestLoading.data)
        assertEquals("Loading More", dataTestLoadingMore.data)
        assertEquals("Success", dataTestSuccess.data)
        assertEquals("Done", dataTestDone.data)
    }

    fun testGetMessage() {
        assertNull(dataTestLoading.message)
        assertNull(dataTestLoadingMore.message)
        assertNull(dataTestSuccess.message)
        assertNull(dataTestDone.message)
        assertEquals("Error Message", dataTestError.message)
    }
}