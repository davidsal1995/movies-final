package com.example.domain.di

import com.example.domain.interactor.auth.DeleteSessionUseCase
import com.example.domain.interactor.auth.GetAccountUseCase
import com.example.domain.interactor.auth.GetSessionUseCase
import com.example.domain.interactor.auth.GetTokenUseCase
import com.example.domain.interactor.show.*
import org.koin.dsl.module

/**
 * Koin module for creating the use case dependencies
 */
val interactionModule = module {
    /**
     * Receives injection: AuthRepository
     * Injected into: LoginActivityViewModel
     */
    factory { GetTokenUseCase(get()) }

    /**
     * Receives injection: AuthRepository
     * Injected into: LoginWebViewModel
     */
    factory { GetSessionUseCase(get()) }

    /**
     * Receives injection: AuthRepository
     * Injected into: ProfileViewModel
     */
    factory { DeleteSessionUseCase(get()) }

    /**
     * Receives injection: AuthRepository
     * Injected into: ShowDetailsViewModel
     */
    factory { GetAccountUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowListViewModel
     */
    factory { GetTopRatedUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowListViewModel
     */
    factory { GetPopularUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowListViewModel
     */
    factory { GetOnTvUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowListViewModel
     */
    factory { GetAiringTodayUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowDetailsViewModel
     */
    factory { GetShowDetailsUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: SeasonDetailsViewModel
     */
    factory { GetSeasonDetailsUseCase(get()) }

    /**
     * Receives injection: ShowRepository
     * Injected into: ShowDetailsViewModel, ProfileViewModel
     */
    factory { SetShowFavoriteStateUseCase(get()) }

    /**
     * Receives injection : ShowRepository
     * Injected into : ShowDetailsViewModel
     */
    factory { GetShowFavoriteStateUseCase(get()) }

    /**
     * Receives injection : ShowRepository
     * Injected into : ShowListViewModel
     */
    factory { GetAccountFavoritesUseCase(get()) }

    /**
     * Receives injection : ShowRepository
     * Injected into : ShowListViewModel
     */
    factory { InsertShowListUseCase(get()) }

    /**
     * Receives injection : ShowRepository
     * Injected into : ShowListViewModel
     */
    factory { GetShowListTagAll(get()) }
}