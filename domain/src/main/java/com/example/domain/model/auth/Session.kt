package com.example.domain.model.auth

/**
 * Data class for holding a TMDB session details
 */
data class Session(
    val success: Boolean,
    val session_id: String
)