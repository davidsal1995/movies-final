package com.example.domain.model.auth

/**
 * JSON Body for the delete session request
 */
data class LogOutRequestBody(
    val session_id: String
)
