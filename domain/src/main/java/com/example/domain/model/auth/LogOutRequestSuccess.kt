package com.example.domain.model.auth

/**
 * Data class for storing the response of the log out request
 */
data class LogOutRequestSuccess(
    val success: Boolean
)
