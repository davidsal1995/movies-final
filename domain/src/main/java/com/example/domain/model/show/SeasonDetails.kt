package com.example.domain.model.show

/**
 * Data class for holding a season details
 */
data class SeasonDetails(
    val name: String,
    val episodes: List<EpisodeDetails>,
    val posterPath: String
)
