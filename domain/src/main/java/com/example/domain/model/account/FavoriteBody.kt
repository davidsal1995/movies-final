package com.example.domain.model.account

/**
 * Data class for storing the JSON post request for favorite setting
 */
data class FavoriteBody(
    val media_id: Int,
    val favorite: Boolean,
    val media_type: String = "tv"
)
