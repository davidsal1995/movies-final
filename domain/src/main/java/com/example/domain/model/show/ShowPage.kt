package com.example.domain.model.show

/**
 * Data class for holding a pagination details
 */
data class ShowPage(
    val page: Int,
    val results: List<ShowEntry>,
    val totalPages: Int,
    val totalResults: Int
)
