package com.example.domain.model.auth

/**
 * Data class for holding a TMDB token
 */
data class Token(
    val success: Boolean,
    val expires_at: String,
    val request_token: String
)