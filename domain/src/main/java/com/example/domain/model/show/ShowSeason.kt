package com.example.domain.model.show

/**
 * Data class for holding a season details
 */
data class ShowSeason(
    val episodeCount: Int,
    val name: String,
    val overview: String,
    val posterPath: String,
    val seasonNumber: Int
)
