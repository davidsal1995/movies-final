package com.example.domain.model.show

/**
 * Data class for storing the favorite state of a show
 */
data class ShowFavoriteState(
    val favorite: Boolean
)
