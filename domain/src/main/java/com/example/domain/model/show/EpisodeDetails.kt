package com.example.domain.model.show

/**
 * Data class for holding an episode details
 */
data class EpisodeDetails(
    val episodeNumber: Int,
    val overview: String,
    val stillPath: String
)