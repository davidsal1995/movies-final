package com.example.domain.model.account

/**
 * Data class for storing the success of setting a show favorite
 */
data class SetFavorite(
    val success: Boolean
)
