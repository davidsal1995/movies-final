package com.example.domain.model.show

/**
 * Data class for holding a show details
 */
data class ShowDetails(
    val id: Int,
    val name: String,
    val originalName: String,
    val voteAverage: Double,
    val overview: String,
    val seasons: List<ShowSeason>,
    val posterPath: String,
    val backDropPath: String
)
