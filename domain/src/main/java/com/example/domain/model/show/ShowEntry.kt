package com.example.domain.model.show

/**
 * Data class for holding a show entry from a pagination
 */
data class ShowEntry(
    val id: Int,
    val posterPath: String,
    val name: String,
    val voteAverage: Double
)