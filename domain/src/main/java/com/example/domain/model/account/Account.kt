package com.example.domain.model.account

/**
 * TMDB profile image
 */
data class AvatarTMDB(
    val avatarPath: String
)

/**
 * TMDB avatar
 */
data class Avatar(
    val tmdb: AvatarTMDB
)

/**
 * TMDB account details
 */
data class Account(
    val name: String,
    val id: Int,
    val avatar: Avatar,
    val username: String,
)
