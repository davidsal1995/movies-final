package com.example.domain.model

/**
 * Data class for holding the data retrieval status
 * @param status is the data status
 * @param data is the actual data
 * @param message is a message in case something goes wrong
 */
data class DataResource<out T>(val status: Status, val data: T? = null, val message: String?) {

    enum class Status(val value: String) {
        SUCCESS("Success"),
        ERROR("Error"),
        LOADING("Loading"),
        LOADING_MORE("Loading More"),
        DONE("Done")
    }

    companion object {
        /**
         * Successful data retrieval
         */
        fun <T> success(data: T): DataResource<T> {
            return DataResource(Status.SUCCESS, data, null)
        }

        /**
         * Error on data retrieval
         */
        fun <T> error(message: String = "", data: T? = null): DataResource<T> {
            return DataResource(Status.ERROR, data, message)
        }

        /**
         * First time data loading
         */
        fun <T> loading(data: T? = null): DataResource<T> {
            return DataResource(Status.LOADING, data, null)
        }

        /**
         * Subsequent data loadings
         */
        fun <T> loadingMore(data: T? = null): DataResource<T> {
            return DataResource(Status.LOADING_MORE, data, null)
        }

        /**
         * Completed all the data retrievals
         */
        fun <T> done(data: T): DataResource<T> {
            return DataResource(Status.DONE, data, null)
        }
    }
}
