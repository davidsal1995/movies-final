package com.example.domain.model.account

import com.example.domain.model.show.ShowDetails

/**
 * Data class for storing the model of the account's favorite list
 */
data class AccountFavorites(
    val results: List<ShowDetails>
)
