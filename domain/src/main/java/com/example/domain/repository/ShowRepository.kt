package com.example.domain.repository

import com.example.domain.model.DataResource
import com.example.domain.model.account.AccountFavorites
import com.example.domain.model.account.FavoriteBody
import com.example.domain.model.account.SetFavorite
import com.example.domain.model.show.*

/**
 * Interface that defines available operations by the ShowRepository
 */
interface ShowRepository {
    // NETWORK //

    /**
     * Get top rated shows from TMDB
     * @param page is the pagination number
     */
    suspend fun getTopRated(page: Int): DataResource<ShowPage>

    /**
     * Get popular shows from TMDB
     * @param page is the pagination number
     */
    suspend fun getPopular(page: Int): DataResource<ShowPage>

    /**
     * Get on tv shows from TMDB
     * @param page is the pagination number
     */
    suspend fun getOnTv(page: Int): DataResource<ShowPage>

    /**
     * Get shows airing today from TMDB
     * @param page is the pagination number
     */
    suspend fun getAiringToday(page: Int): DataResource<ShowPage>

    /**
     * Get a show details from TMDB
     * @param id is the show id
     */
    suspend fun getShowDetails(id: Int): DataResource<ShowDetails>

    /**
     * Get a season details from TMDB
     * @param id is the show id
     * @param number is the season number
     */
    suspend fun getSeasonDetails(id: Int, number: Int): DataResource<SeasonDetails>

    /**
     * Get the favorite state of a show
     * @param id is the show id
     * @param session is the session id
     */
    suspend fun getShowFavoriteState(id: Int, session: String): DataResource<ShowFavoriteState>

    /**
     * Sets the favorite state of a show
     * @param account is the TMDB account
     * @param session is the TMDB session
     * @param body is the JSON POST data
     */
    suspend fun setShowFavoriteState(
        account: Int,
        session: String,
        body: FavoriteBody
    ): DataResource<SetFavorite>

    /**
     * Get the favorite list of the account
     * @param id is the show id
     * @param session is the session id
     */
    suspend fun getAccountFavorites(id: Int, session: String): DataResource<AccountFavorites>

    //DATABASE//

    /**
     * Gets a list of tv shows from the database
     * @param shows is the tv shows list
     */
    suspend fun insertShowList(shows: List<ShowEntry>, page: Int, tag: String)

    /**
     * Gets a list of all tv shows from the database with the provided tag
     * @param tag is the show tag
     */
    suspend fun getShowListTagAll(tag: String): List<ShowEntry>
}