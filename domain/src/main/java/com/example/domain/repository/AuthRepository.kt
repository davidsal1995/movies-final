package com.example.domain.repository

import com.example.domain.model.DataResource
import com.example.domain.model.account.Account
import com.example.domain.model.auth.LogOutRequestBody
import com.example.domain.model.auth.LogOutRequestSuccess
import com.example.domain.model.auth.Session
import com.example.domain.model.auth.Token

/**
 * Interface that defines operations available for the AuthRepositoryImpl
 */
interface AuthRepository {
    /**
     * Get a TMDB token
     */
    suspend fun getToken(): DataResource<Token>

    /**
     * Get a TMDB session
     * @param token is a TMDB token
     */
    suspend fun getSession(token: String): DataResource<Session>

    /**
     * Get a TMDB account details
     * @param session is a TMDB session
     */
    suspend fun getAccount(session: String): DataResource<Account>

    /**
     * Delete a TMDB session
     * @param body is the request JSON post data
     */
    suspend fun deleteSession(body: LogOutRequestBody): DataResource<LogOutRequestSuccess>
}