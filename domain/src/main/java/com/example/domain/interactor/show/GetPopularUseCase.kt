package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving popular shows
 * @param showRepository is the Show repository
 */
class GetPopularUseCase(private val showRepository: ShowRepository) {
    /**
     * @param page is the pagination number
     */
    suspend operator fun invoke(page: Int) = showRepository.getPopular(page)
}