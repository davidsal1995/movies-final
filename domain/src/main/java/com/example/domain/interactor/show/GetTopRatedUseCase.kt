package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving top rated shows
 * @param showRepository is the Show repository
 */
class GetTopRatedUseCase(private val showRepository: ShowRepository) {
    /**
     * @param page is the pagination number
     */
    suspend operator fun invoke(page: Int) = showRepository.getTopRated(page)
}