package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving shows on tv
 * @param showRepository is the Show repository
 */
class GetOnTvUseCase(private val showRepository: ShowRepository) {
    /**
     * @param page is the pagination number
     */
    suspend operator fun invoke(page: Int) = showRepository.getOnTv(page)
}