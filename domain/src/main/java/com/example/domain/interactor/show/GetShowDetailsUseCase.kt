package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving shows airing today
 * @param showRepository is the Show repository
 */
class GetShowDetailsUseCase(private val showRepository: ShowRepository) {
    /**
     * @param id is the show id
     */
    suspend operator fun invoke(id: Int) = showRepository.getShowDetails(id)
}

