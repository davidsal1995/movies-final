package com.example.domain.interactor.show

import com.example.domain.model.account.FavoriteBody
import com.example.domain.repository.ShowRepository

/**
 * Use case for setting the favorite state of a show
 * @param showRepository is the Show repository
 */
class SetShowFavoriteStateUseCase(private val showRepository: ShowRepository) {
    /**
     *@param account is the TMDB account
     * @param session is the TMDB session
     * @param body is the JSON POST data
     */
    suspend operator fun invoke(account: Int, session: String, body: FavoriteBody) =
        showRepository.setShowFavoriteState(account, session, body)
}