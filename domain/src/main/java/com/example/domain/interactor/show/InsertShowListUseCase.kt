package com.example.domain.interactor.show

import com.example.domain.model.show.ShowEntry
import com.example.domain.repository.ShowRepository

/**
 * Use case for inserting shows into the database
 * @param showRepository is the Show repository
 */
class InsertShowListUseCase(private val showRepository: ShowRepository) {
    /**
     * @param shows is the shows list
     */
    suspend operator fun invoke(shows: List<ShowEntry>, page: Int, tag: String) =
        showRepository.insertShowList(shows, page, tag)
}