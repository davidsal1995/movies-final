package com.example.domain.interactor.auth

import com.example.domain.repository.AuthRepository

/**
 * Use case for retrieving a TMDB session
 * @param authRepository is the Authentication repository
 */
class GetSessionUseCase(private val authRepository: AuthRepository) {
    /**
     * @param token is a TMDB token
     */
    suspend operator fun invoke(token: String) = authRepository.getSession(token)
}