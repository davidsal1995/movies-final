package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving shows airing today
 * @param showRepository is the Show repository
 */
class GetAiringTodayUseCase(private val showRepository: ShowRepository) {
    /**
     * @param page is the pagination number
     */
    suspend operator fun invoke(page: Int) = showRepository.getAiringToday(page)
}