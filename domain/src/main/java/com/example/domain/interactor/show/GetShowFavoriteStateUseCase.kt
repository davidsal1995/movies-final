package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for checking if a show is marked as favorite
 * @param showRepository is the Show repository
 */
class GetShowFavoriteStateUseCase(private val showRepository: ShowRepository) {
    /**
     * @param id is the show id
     */
    suspend operator fun invoke(id: Int, session: String) =
        showRepository.getShowFavoriteState(id, session)
}