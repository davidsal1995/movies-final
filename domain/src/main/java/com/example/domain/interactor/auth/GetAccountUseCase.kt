package com.example.domain.interactor.auth

import com.example.domain.repository.AuthRepository

/**
 * Use case for retrieving a TMDB account details
 * @param authRepository is the Authentication repository
 */
class GetAccountUseCase(private val authRepository: AuthRepository) {
    /**
     * @param session is a TMDB token
     */
    suspend operator fun invoke(session: String) = authRepository.getAccount(session)
}