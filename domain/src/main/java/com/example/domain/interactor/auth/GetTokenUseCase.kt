package com.example.domain.interactor.auth

import com.example.domain.repository.AuthRepository

/**
 * Use case for retrieving a TMDB token
 * @param authRepository is the Authentication repository
 */
class GetTokenUseCase(private val authRepository: AuthRepository) {
    suspend operator fun invoke() = authRepository.getToken()
}