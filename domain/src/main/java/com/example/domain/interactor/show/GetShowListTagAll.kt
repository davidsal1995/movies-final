package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving all the shows from the database with the provided tag
 * @param showRepository is the Show repository
 */
class GetShowListTagAll(private val showRepository: ShowRepository) {
    /**
     * @param tag is the show tag
     */
    suspend operator fun invoke(tag: String) = showRepository.getShowListTagAll(tag)
}