package com.example.domain.interactor.auth

import com.example.domain.model.auth.LogOutRequestBody
import com.example.domain.repository.AuthRepository

/**
 * Use case for retrieving a TMDB account details
 * @param authRepository is the Authentication repository
 */
class DeleteSessionUseCase(private val authRepository: AuthRepository) {
    /**
     * @param body is the JSON data for the request
     */
    suspend operator fun invoke(body: LogOutRequestBody) = authRepository.deleteSession(body)
}