package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving a season details
 * @param showRepository is the Show repository
 */
class GetSeasonDetailsUseCase(private val showRepository: ShowRepository) {
    /**
     * @param id is the show id
     * @param number is the season number
     */
    suspend operator fun invoke(id: Int, number: Int) = showRepository.getSeasonDetails(id, number)
}