package com.example.domain.interactor.show

import com.example.domain.repository.ShowRepository

/**
 * Use case for retrieving the favorite list of the account
 * @param showRepository is the Show repository
 */
class GetAccountFavoritesUseCase(private val showRepository: ShowRepository) {
    /**
     * @param id is the account id
     * @param session is the TMDB session
     */
    suspend operator fun invoke(id: Int, session: String) =
        showRepository.getAccountFavorites(id, session)
}